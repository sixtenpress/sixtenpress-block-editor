<?php

/**
 * Check current theme support and update as needed.
 * Class SixTenPressBlockEditorThemeSupport
 * @copyright 2019-2020 Robin Cornett
 */
class SixTenPressBlockEditorThemeSupport {

	/**
	 * Optionally modify theme support definitions,
	 * but not on the plugin settings page or if we
	 * have just switched themes.
	 */
	public function modify_theme_support() {
		$theme_switched = get_option( 'theme_switched' );
		$setting        = sixtenpressblockeditor_get_setting();
		if ( $theme_switched || empty( $setting['current_theme_support'] ) ) {
			$this->check_current_theme_support();
		}
		$this->disable_custom_options( $setting );
		if ( is_admin() ) {
			$page = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_STRING );
			$tab  = filter_input( INPUT_GET, 'tab', FILTER_SANITIZE_STRING );
			if ( 'sixtenpressblockeditor' === $page ) {
				return;
			}
			if ( 'sixtenpress' === $page && 'blockeditor' === $tab ) {
				return;
			}
		}
		$this->add_theme_support_strings( $setting );
		$this->add_theme_support_arrays( $setting );
	}

	/**
	 * Optionally disable custom block editor features.
	 * Currently: custom colors, custom font sizes
	 * @since 0.4.0
	 *
	 * @param $setting
	 */
	private function disable_custom_options( $setting ) {
		$disable = array(
			'disable_custom_colors'    => 'disable-custom-colors',
			'disable_font_sizes'       => 'disable-custom-font-sizes',
			'disable_custom_gradients' => 'disable-custom-gradients',
		);
		foreach ( $disable as $key => $feature ) {
			if ( ! empty( $setting[ $key ] ) ) {
				add_theme_support( $feature );
			}
		}
	}

	/**
	 * Optionally add theme supports for block editor features set as strings.
	 * Currently: wide/full alignment, responsive embeds
	 * @since 0.4.0
	 *
	 * @param $setting
	 */
	private function add_theme_support_strings( $setting ) {
		$support = array(
			'align_wide'        => 'align-wide',
			'responsive_embeds' => 'responsive-embeds',
		);
		foreach ( $support as $key => $value ) {
			if ( get_theme_support( $value ) ) {
				continue;
			}
			if ( ! empty( $setting[ $key ] ) ) {
				add_theme_support( $value );
			}
		}
	}

	/**
	 * Optionally add theme supports for block editor features set as arrays.
	 * Currently: color palette, font sizes
	 * @since 0.4.0
	 *
	 * @param $setting
	 */
	private function add_theme_support_arrays( $setting ) {
		$features = array(
			'colors' => 'editor-color-palette',
			'fonts'  => 'editor-font-sizes',
		);
		foreach ( $features as $key => $feature ) {
			if ( empty( $setting[ $key ] ) ) {
				continue;
			}
			$support = $this->update_theme_support( $feature, $setting[ $key ] );
			if ( $support ) {
				add_theme_support( $feature, $support );
			}
		}
	}

	/**
	 * Get the custom editor colors--the theme colors merged with the plugin setting.
	 *
	 * @param string $feature
	 * @param array  $setting
	 *
	 * @return array
	 */
	private function update_theme_support( $feature, $setting = array() ) {
		$theme   = sixtenpressblockeditor_get_theme_support_array( $feature );
		$setting = $this->maybe_unset_settings( $setting );
		if ( empty( $theme ) || is_string( $theme ) ) {
			return 'editor-font-sizes' === $feature ? $this->update_font_sizes( $setting ) : $setting;
		}
		$merged       = array_merge( $theme, $setting );
		$array_unique = array_unique( array_column( $merged, 'slug' ) );
		$array_unique = array_filter( $array_unique );

		$theme_support = array_values( array_intersect_key( $merged, $array_unique ) );
		if ( 'editor-font-sizes' !== $feature ) {
			return $theme_support;
		}
		if ( ! is_admin() ) {
			return $theme_support;
		}
		$theme_support = $this->update_font_sizes( $theme_support );
		if ( array_search( 'normal', array_column( $theme_support, 'slug' ), true ) ) {
			return $theme_support;
		}

		return array_merge(
			array(
				array(
					'name' => __( 'Normal', 'sixtenpress-block-editor' ),
					'size' => 16,
					'slug' => 'normal',
				),
			),
			$theme_support
		);
	}

	/**
	 * Update font sizes for the editor, if needed.
	 * Converts strings to integers, and px/em strings to relative integers in pixels.
	 *
	 * @param array $font_sizes
	 * @return array
	 * @since 0.9.1
	 */
	private function update_font_sizes( $font_sizes ) {
		foreach ( $font_sizes as $key => &$value ) {
			if ( is_numeric( $value['size'] ) ) {
				$font_sizes[ $key ]['size'] = (int) $value['size'];
				continue;
			}
			if ( false !== strpos( $value['size'], 'em' ) ) {
				$number                     = str_replace( 'em', '', $value['size'] );
				$font_sizes[ $key ]['size'] = 16 * (int) $number;
				continue;
			}
			if ( false !== strpos( $value['size'], 'px' ) ) {
				$number                     = str_replace( 'px', '', $value['size'] );
				$font_sizes[ $key ]['size'] = (int) $number;
				continue;
			}
		}

		return $font_sizes;
	}

	/**
	 * Zip through the setting and unset any invalid values.
	 *
	 * @param array $setting
	 * @return array
	 * @since 0.9.1
	 */
	private function maybe_unset_settings( $setting ) {
		foreach ( $setting as $key => &$value ) {
			if ( empty( $value['slug'] ) ) {
				unset( $setting[ $key ] );
			}
		}

		return $setting;
	}

	/**
	 * Note if the current theme supports certain features.
	 * @since 0.5.0
	 */
	private function check_current_theme_support() {
		$new = array(
			'current_theme_support' => array(
				'align_wide'        => get_theme_support( 'align-wide' ),
				'responsive_embeds' => get_theme_support( 'responsive-embeds' ),
			),
		);
		$old = get_option( 'sixtenpressblockeditor' );

		update_option( 'sixtenpressblockeditor', wp_parse_args( $new, $old ) );
	}
}
