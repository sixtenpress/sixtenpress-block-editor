; ( function ( wp, $, undefined ) {
	'use strict';

	var SixTenBlock = {},
		id = '#_sixtenpressblockeditor';

	/**
	 * Remove styles.
	 *@since 0.8.0
	 */
	function _removeStyles () {
		if ( !SixTenBlock.params.remove_styles ) {
			return;
		}
		$( SixTenBlock.params.remove_styles ).each( function () {
			wp.blocks.unregisterBlockStyle( this.block, this.name );
		} );
	}

	/**
	 * Add new styles.
	 * @since 0.8.0
	 */
	function _addStyles () {
		if ( !SixTenBlock.params.new_styles ) {
			return;
		}
		$( SixTenBlock.params.new_styles ).each( function () {
			wp.blocks.registerBlockStyle( this.block, {
				name: this.name,
				label: this.label,
			} );
		} );
	}

	/**
	 * Users can optionally register a list of blocks to unregister.
	 */
	function _removeBlocks () {
		var disallowedBlocks = SixTenBlock.params.disallow;
		if ( !disallowedBlocks ) {
			return;
		}
		disallowedBlocks.forEach( function ( blockName ) {
			var allBlocks = wp.blocks.getBlockTypes().map( a => a.name );
			if ( allBlocks.indexOf( blockName ) !== -1 ) {
				wp.blocks.unregisterBlockType( blockName );
			}
		} );
	}

	/**
	 * Add custom classes to the block areas editor.
	 * @private
	 */
	function _doCustomClasses () {
		var background = document.querySelector( id + '-background_hex' ),
			color = document.querySelector( id + '-color_hex' );
		if ( !background ) {
			return;
		}
		var root = document.querySelector( '.editor-styles-wrapper' ),
			backgroundValue = $( background ).val(),
			colorValue = $( color ).val();
		_update_editor_style_from_picker( root );
		if ( backgroundValue ) {
			$( root ).css( 'background-color', backgroundValue );
		}
		if ( colorValue ) {
			$( root ).css( 'color', colorValue );
		}
	}

	/**
	 * Update the editor wrapper style based on the Iris color picker changes.
	 * @param root
	 */
	function _update_editor_style_from_picker ( root ) {
		$( id + '-background_hex' ).wpColorPicker( {
			change: function ( event, ui ) {
				$( root ).css( 'background-color', ui.color.toString() );
			},
			clear: function ( event, ui ) {
				$( root ).css( 'background-color', '' );
			}
		} );
		$( id + '-color_hex' ).wpColorPicker( {
			change: function ( event, ui ) {
				$( root ).css( 'color', ui.color.toString() );
			},
			clear: function ( event, ui ) {
				$( root ).css( 'color', '' );
			}
		} );
	}

	wp.domReady( function () {
		SixTenBlock.params = typeof SixTenPressBlockEditorConfig === 'undefined' ? '' : SixTenPressBlockEditorConfig;
		if ( typeof SixTenBlock.params !== 'undefined' ) {
			setTimeout( function () {
				_doCustomClasses();
				_addStyles();
			}, 1000 );
			_removeStyles();
			_removeBlocks();
		}
	} );
} )( wp, jQuery );
