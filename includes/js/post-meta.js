/*
 * Copyright (c) 2017 Robin Cornett
 */

; ( function ( document, $, undefined ) {
	'use strict';

	var SixTenRepeat = {},
		table = '.sixten-meta',
		buttons = table + ' .repeatable-buttons',
		repeater = '.repeater-container',
		repeatable = '.repeatable';

	/**
	 * Set up repeatable rows and enable sorting.
	 */
	SixTenRepeat.init = function () {
		var hasRepeaters = $( table ).find( repeatable );
		if ( hasRepeaters.length === 0 ) {
			return;
		}

		SixTenRepeat.required();
		SixTenRepeat.addButtons();
		SixTenRepeat.maybeDisable();

		$( table + ' ' + repeater ).sortable( {
			axis: 'y',
			containment: 'parent',
			handle: '.move-group',
			items: repeatable,
			tolerance: 'pointer',
			start: function ( event, ui ) {
				var id = $( ui.item ).find( '.wp-editor-area' ).attr( 'id' );
				tinymce.execCommand( 'mceRemoveEditor', true, id );
			},
			stop: function ( event, ui ) {
				var id = $( ui.item ).find( '.wp-editor-area' ).attr( 'id' );
				tinymce.execCommand( 'mceAddEditor', true, id );
				SixTenRepeat.updateShift();
				SixTenRepeat.maybeRename();
			}
		} );

		$( buttons + ' .add-group' ).on( 'click.add-group', SixTenRepeat.clone );
		$( buttons + ' .remove-group' ).on( 'click.remove-group', SixTenRepeat.remove );
		$( buttons + ' .clear-group' ).on( 'click.clear-group', SixTenRepeat.clear );
		$( table + ' .toggle' ).on( 'click.toggle', SixTenRepeat.toggle );
		$( table + ' .shift' ).on( 'click.shift', SixTenRepeat.shift );
	};

	/**
	 * Remove required property from hidden fields.
	 */
	SixTenRepeat.required = function () {
		var $last = $( table + ' ' + repeater + ' .row:last-child' ),
			required = $last.find( 'div.required' );

		$( required ).find( 'input' ).attr( 'required', false );
	};

	/**
	 * Fix tinyMCE editor saving in Gutenberg/block editor.
	 * Code from CMB2.
	 */
	SixTenRepeat.blockEditor = function () {
		if ( !wp.data || !window.tinymce ) {
			return;
		}
		wp.data.subscribe( function () {
			if ( wp.data.select( 'core/editor' ).isSavingPost() && window.tinymce.editors ) {
				for ( var i = 0; i < tinymce.editors.length; i++ ) {
					if ( tinymce.activeEditor !== tinymce.editors[ i ] ) {
						tinymce.editors[ i ].save();
					}
				}
			}
		} );
	};

	/**
	 * Create and add necessary row buttons.
	 */
	SixTenRepeat.addButtons = function () {
		var clearfix = $( '<div />', {
			class: 'clearfix'
		} ),
			container = $( '<div />', {
				class: 'repeatable-buttons'
			} ).append( SixTenRepeat.getButtons( SixTenRepeat.params.buttons ) );

		$( table + ' ' + repeatable + ' > .input' ).append( clearfix ).append( container );
		$( table + ' .can-toggle' ).wrapInner( '<div class="toggle-container" />' ).parents( '.group' ).find( '> .label' ).append( SixTenRepeat.makeButton( SixTenRepeat.params.toggle ) );
		$( table + ' ' + repeatable + ' > .label' ).append( SixTenRepeat.makeButton( SixTenRepeat.params.move ) );
	};

	/**
	 * Get grouped buttons.
	 * @returns {Array}
	 */
	SixTenRepeat.getButtons = function ( buttons ) {
		var groupButtons = [];
		Object.keys( buttons ).forEach( function ( key ) {
			if ( buttons.hasOwnProperty( key ) ) {
				groupButtons.push( SixTenRepeat.makeButton( buttons[ key ] ) );
			}
		} );
		return groupButtons;
	};

	/**
	 * Create a button (used for add/remove, and toggle).
	 *
	 * @param button
	 */
	SixTenRepeat.makeButton = function ( button ) {
		var element = 'move-group' === button.class ? 'div' : 'button';
		return $( '<' + element + ' />', {
			class: button.class + ' button-secondary'
		} ).append( $( '<span />', {
			class: 'dashicons ' + button.icon
		} ) ).append( $( '<span />', {
			class: 'button-label',
			text: button.label
		} ) );
	};

	/**
	 * If a toggle button exists for a hidden section, toggle it.
	 * @param e
	 */
	SixTenRepeat.toggle = function ( e ) {
		e.preventDefault();
		$( this ).parents( '.group' ).find( '.section.can-toggle' ).slideToggle();
	};

	/**
	 * Clone the last row.
	 *
	 * @param e
	 */
	SixTenRepeat.clone = function ( e ) {
		e.preventDefault();
		var $this = $( this ),
			$group = $this.closest( repeatable ),
			$last = $group.siblings().last().data( 'arr', [ 1 ] );

		SixTenRepeat.tinymceRemove( $last );

		// Clone and clean up the clone.
		var cloned = $last.clone( true, true )
			.data( 'arr', $.extend( [], $last.data( 'arr' ) ) );
		SixTenRepeat.housekeeping( cloned );
		cloned.insertAfter( $group );
		cloned.find( '.required input' ).prop( 'required', true );
		SixTenRepeat.tinymce( cloned );
		setTimeout( function () {
			SixTenRepeat.maybeRename();
		}, 300 );
		SixTenRepeat.tinymce( $last );

		// Check if the remove button needs to be disabled.
		SixTenRepeat.maybeDisable();
	};

	/**
	 * Remove a row.
	 *
	 * @param e
	 */
	SixTenRepeat.remove = function ( e ) {
		e.preventDefault();
		var confirmation = confirm( SixTenRepeat.params.confirm );
		if ( confirmation === true ) {
			var $this = $( this ),
				$group = $this.closest( repeatable );
			$group.remove();
		}
		SixTenRepeat.maybeDisable();
		setTimeout( function () {
			SixTenRepeat.maybeRename();
		}, 300 );
	};

	/**
	 * Clear data if the first row is the only row and the Remove button is clicked.
	 */
	SixTenRepeat.clear = function ( e ) {
		e.preventDefault();
		var confirmation = confirm( SixTenRepeat.params.confirm );
		if ( confirmation !== true ) {
			return;
		}
		var $this = $( this ).closest( repeatable );
		$this.find( 'input, select' ).val( '' );
		$this.find( 'input[type=checkbox]' ).attr( 'checked', false );
		$this.find( '.wp-color-result' ).css( 'background-color', '' );
	}

	/**
	 * Shift rows up or down by click.
	 *
	 * @param e
	 */
	SixTenRepeat.shift = function ( e ) {
		e.preventDefault();
		var $this = $( this ),
			$group = $this.parents( repeatable ),
			action = 'next',
			insert = 'insertAfter',
			top = '-' + $group.height();

		if ( $this.hasClass( 'shift-up' ) ) {
			action = 'prev';
			insert = 'insertBefore';
			top = $group.height();
		}

		var $row = $group[ action ]( repeatable ),
			height = $this.hasClass( 'shift-up' ) ? '-' + $row.height() : $row.height();

		if ( $row.length === 0 ) {
			return;
		}

		$row.css( 'z-index', 999 )
			.css( 'position', 'relative' )
			.animate( { top: top }, 250 );
		$group.css( 'z-index', 1000 )
			.css( 'position', 'relative' )
			.animate( { top: height }, 300, function () {
				$row.css( 'z-index', '' )
					.css( 'top', '' )
					.css( 'position', '' );
				$group.css( 'z-index', '' )
					.css( 'top', '' )
					.css( 'position', '' );
				$group[ insert ]( $row );
			} );

		var editor = $group.find( '.wp-editor-area' ).attr( 'id' );
		if ( editor !== 'undefined' ) {
			tinymce.EditorManager.execCommand( 'mceRemoveEditor', true, editor );
			setTimeout( function () {
				tinymce.EditorManager.execCommand( 'mceAddEditor', true, editor );
				SixTenRepeat.updateShift();
			}, 300 );
		}

		setTimeout( function () {
			SixTenRepeat.maybeRename();
		}, 300 );
	};

	/**
	 * Check and disable or re-enable the remove button.
	 */
	SixTenRepeat.maybeDisable = function () {
		$( table ).find( repeater ).each( function () {
			var $this = $( this ),
				one_row = 2 === $this.find( repeatable ).length,
				clearButtonToggle = one_row ? 'show' : 'hide',
				moveButtonToggle = one_row ? 'hide' : 'show';
			$this.find( '.remove-group, .move-group' )[ moveButtonToggle ]();
			$this.find( '.clear-group' )[ clearButtonToggle ]();
		} );
		SixTenRepeat.updateShift();
	};

	/**
	 * Show/hide shift buttons as needed.
	 */
	SixTenRepeat.updateShift = function () {
		$( table ).find( repeater ).each( function () {
			var $this = $( this ),
				row = $this.find( repeatable ),
				shift = $this.find( repeatable + ' .shift' ),
				last = $this.find( repeatable + ':nth-last-child(2)' );
			$( shift ).show();
			$( row ).first().find( '.shift-up' ).hide();
			$( last ).find( '.shift-down' ).hide();
		} );
	};

	/**
	 * Initialize the color picker.
	 *
	 * @param $selector
	 */
	SixTenRepeat.initColorPickers = function ( $selector ) {
		if ( !$selector.length ) {
			return;
		}

		if ( typeof jQuery.wp === 'object' && typeof jQuery.wp.wpColorPicker === 'function' ) {

			$selector.each( function () {
				var $this = $( this ),
					settings = $this.data( 'colorpicker' ) || {};
				$this.wpColorPicker( $.extend( {}, SixTenRepeat.params.color_picker, settings ) );
			} );
		}
	};

	/**
	 * Tidy up the color picker field before re-initializing.
	 *
	 * @param $row
	 */
	SixTenRepeat.housekeeping = function ( $row ) {
		var $colorPicker = $row.find( '.wp-picker-container' );

		if ( $colorPicker.length ) {
			$colorPicker.each( function () {
				var $td = $( this ).parent();
				$td.html( $td.find( 'input[type="text"].color-field' ).attr( 'style', '' ) );
			} );
		}
		SixTenRepeat.initColorPickers( $row.find( 'input[type="text"].wp-color-picker' ) );
	};

	/**
	 * Remove the nonfunctional tinymce div and create a new one.
	 * Works, but feels strange.
	 *
	 * @param $row
	 */
	SixTenRepeat.tinymce = function ( $row ) {
		var editor = $row.find( '.wp-editor-area' ).attr( 'id' );
		if ( typeof editor === 'undefined' ) {
			return;
		}
		var original = $row.find( '.mce-tinymce.mce-panel' ).attr( 'id' );
		$( '#' + original ).remove();
		tinymce.EditorManager.execCommand( 'mceRemoveEditor', true, editor );
		tinymce.EditorManager.execCommand( 'mceAddEditor', true, editor );
		$( '.mce-tinymce.mce-panel' ).show();
	};

	/**
	 * Remove the editor from a row.
	 *
	 * @param $row
	 */
	SixTenRepeat.tinymceRemove = function ( $row ) {
		var editor = $row.find( '.wp-editor-area' ).attr( 'id' );
		if ( typeof editor === 'undefined' ) {
			return;
		}
		tinymce.EditorManager.execCommand( 'mceRemoveEditor', true, editor );
	};

	/**
	 * Maybe cycle through and rename inputs.
	 * @private
	 */
	SixTenRepeat.maybeRename = function () {
		$( table + ' ' + repeater + ' ' + repeatable ).each( function ( row ) {
			var current = $( this ).attr( 'data-iterator' );
			if ( row === parseInt( current ) ) {
				return true;
			}
			$( this ).attr( 'data-iterator', row );
			$( this ).find( ':input' ).each( function () {
				SixTenRepeat.reiterateRow( $( this ), 'name', current, row );
				SixTenRepeat.reiterateRow( $( this ), 'id', current, row );
			} );
			$( this ).find( 'label' ).each( function () {
				SixTenRepeat.reiterateRow( $( this ), 'for', current, row );
			} );
		} );
	};

	/**
	 * For a given element and attribute, update the attribute
	 * from the previous row number to the new.
	 *
	 * @param element
	 * @param attribute
	 * @param current
	 * @param row
	 */
	SixTenRepeat.reiterateRow = function ( element, attribute, current, row ) {
		var name = element.attr( attribute );
		if ( undefined === name ) {
			return;
		}
		name = name.replace( current, row );
		element.attr( attribute, name );
		if ( 'name' === attribute ) {
			element.attr( 'aria-label', name );
		}
	};

	SixTenRepeat.params = typeof SixTenRepeaters === 'undefined' ? '' : SixTenRepeaters;
	if ( typeof SixTenRepeat.params !== 'undefined' ) {
		SixTenRepeat.init();
		SixTenRepeat.blockEditor();
	}

} )( document, jQuery );
