<?php

$description   = __( 'Optionally add new colors to your site\'s theme color palette. Any colors defined by your theme or here will show in the block editor.', 'sixtenpress-block-editor' );
$theme_support = sixtenpressblockeditor_get_theme_support_array( 'editor-color-palette' );
if ( ! is_array( $theme_support ) ) {
	$description .= ' ' . __( 'Your theme doesn\'t define a color palette, so it will use the custom colors provided by WordPress.', 'sixtenpress-block-editor' );
	$description .= ' ' . __( 'However, once you define colors here, you\'ll be replacing the WordPress defaults.', 'sixtenpress-block-editor' );

	return $description;
}
$description .= ' ' . __( 'Looks like your theme has already defined the following colors:', 'sixtenpress-block-editor' );
$description .= '<table class="sixtenpress-blockeditor-small-table"><tbody>';
$description .= sprintf(
	'<tr><th>%s</th><th>%s</th><th>%s</th></tr>',
	__( 'Name', 'sixtenpress-block-editor' ),
	__( 'Slug', 'sixtenpress-block-editor' ),
	__( 'Color', 'sixtenpress-block-editor' )
);
foreach ( $theme_support as $color ) {
	$description .= sprintf(
		'<tr><td data-label="%4$s">%1$s</td><td data-label="%5$s">%2$s</td><td data-label="%6$s">%3$s<span class="color-sample color-sample--%2$s"></span></td></tr>',
		$color['name'],
		$color['slug'],
		$color['color'],
		__( 'Name', 'sixtenpress-block-editor' ),
		__( 'Slug', 'sixtenpress-block-editor' ),
		__( 'Color', 'sixtenpress-block-editor' )
	);
}
$description .= '</tbody></table>';

return $description;
