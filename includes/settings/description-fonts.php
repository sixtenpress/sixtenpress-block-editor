<?php

$description   = __( 'Optionally add new font sizes to your site\'s theme. Any sizes defined by your theme or here will show in the block editor.', 'sixtenpress-block-editor' );
$theme_support = sixtenpressblockeditor_get_theme_support_array( 'editor-font-sizes' );
if ( ! $theme_support ) {
	$description .= ' ' . __( 'Your theme doesn\'t define custom font sizes, so it will use the custom font sizes provided by WordPress. However, once you define sizes here, you\'ll be replacing the WordPress defaults.', 'sixtenpress-block-editor' );
	$description .= ' ' . __( 'However, once you define sizes here, you\'ll be replacing the WordPress defaults.', 'sixtenpress-block-editor' );

	return $description;
}
$description .= ' ' . __( 'Looks like your theme has already defined the following font sizes:', 'sixtenpress-block-editor' );
$description .= '<table class="sixtenpress-blockeditor-small-table"><tbody>';
$description .= sprintf(
	'<tr><th>%s</th><th>%s</th><th>%s</th></tr>',
	__( 'Name', 'sixtenpress-block-editor' ),
	__( 'Slug', 'sixtenpress-block-editor' ),
	__( 'Size', 'sixtenpress-block-editor' )
);
foreach ( $theme_support as $size ) {
	$description .= sprintf(
		'<tr><td data-label="%4$s">%1$s</td><td data-label="%5$s">%2$s</td><td data-label="%6$s">%3$s</td></tr>',
		$size['name'],
		$size['slug'],
		$size['size'],
		__( 'Name', 'sixtenpress-block-editor' ),
		__( 'Slug', 'sixtenpress-block-editor' ),
		__( 'Size', 'sixtenpress-block-editor' )
	);
}
$description .= '</tbody></table>';

return $description;
