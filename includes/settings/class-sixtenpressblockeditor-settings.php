<?php

/**
 * Class SixTenPressBlockEditorSettings
 * @copyright 2019-2020 Robin Cornett
 */
class SixTenPressBlockEditorSettings extends SixTenPressSettings {

	/**
	 * String for the page/section/setting.
	 * @var string $page
	 */
	protected $page = 'sixtenpress';

	/**
	 * String for the tab.
	 * @var string
	 */
	protected $tab = 'sixtenpressblockeditor';

	/**
	 * Define the setting key.
	 * @return array
	 */
	protected function define_setting() {
		return include 'defaults.php';
	}

	/**
	 * Get the plugin setting, merged with defaults.
	 * @return array
	 */
	public function get_setting() {
		$setting  = get_option( $this->tab, array() );
		$defaults = $this->define_setting();

		return wp_parse_args( $setting, $defaults );
	}

	/**
	 * Maybe add a new settings page.
	 */
	public function maybe_add_settings_page() {
		$this->maybe_add_option();
		$this->setting = $this->get_setting();
		add_action( 'admin_init', array( $this, 'register' ) );
		$this->enqueue_all();

		if ( ! $this->is_sixten_active() ) {
			$this->page = $this->tab;
			add_options_page(
				__( '6/10 Press Block Editor Settings', 'sixtenpress-block-editor' ),
				__( '6/10 Press Block Editor', 'sixtenpress-block-editor' ),
				'manage_options',
				$this->page,
				array( $this, 'do_simple_settings_form' )
			);
		}
		$this->action = "{$this->page}_save-settings";
		$this->nonce  = "{$this->page}_nonce";

		add_filter( 'sixtenpress_settings_tabs', array( $this, 'add_tab' ) );
		add_action( "load-settings_page_{$this->page}", array( $this, 'build_settings_page' ) );
	}

	/**
	 * Checking this preemptively and adding a setting out of the gate because
	 * WP saves the option twice on the first go and loses the last color/font.
	 */
	private function maybe_add_option() {
		$option = get_option( $this->tab );
		if ( $option ) {
			return;
		}
		add_option( $this->tab, $this->define_setting() );
	}

	/**
	 * Add the sections and settings to the settings tab/page.
	 */
	public function build_settings_page() {
		if ( ! $this->is_sixten_active() ) {
			include_once 'class-sixtenpressblockeditor-settings-enqueue.php';
			new SixTenPressBlockEditorSettingsEnqueue();
		}
		add_action( 'admin_enqueue_scripts', array( $this, 'add_inline_style' ), 50 );
		$sections = $this->define_section();
		$this->add_sections( $sections );
		$this->add_fields( $this->register_fields(), $sections );
	}

	/**
	 * Add styling for the colors/fonts tables.
	 * @since 0.3.0
	 */
	public function add_inline_style() {
		wp_add_inline_style( 'sixtenpress-postmeta', include 'inline-style.php' );
	}

	/**
	 * Description for the custom colors group field.
	 * @since 0.3.0
	 *
	 * @return string
	 */
	protected function colors_field_description() {
		return include 'description-colors.php';
	}

	/**
	 * Description for the custom font sizes group field.
	 * @since 0.3.0
	 *
	 * @return string
	 */
	protected function fonts_field_description() {
		return include 'description-fonts.php';
	}

	/**
	 * Conditionally modify the group field label on the colors palette to show warnings about theme defined colors.
	 * @param $field
	 * @param $i
	 * @param $parent
	 */
	public function modify_group_field_label( $field, $i, $parent ) {
		if ( 'colors' !== $field['id'] ) {
			return;
		}
		$this->do_subfield_description( 'editor-color-palette', $field, $i, 'color', 'color' );
	}

	/**
	 * Conditionally modify the group field label on the font sizes to show warnings about theme defined sizes.
	 * @param $field
	 * @param $i
	 * @param $parent
	 */
	public function modify_fonts_field_label( $field, $i, $parent ) {
		if ( 'fonts' !== $field['id'] ) {
			return;
		}
		$this->do_subfield_description( 'editor-font-sizes', $field, $i, 'font', 'size' );
	}

	/**
	 * Print the subfield description if it is needed.
	 *
	 * @param string $theme_supports
	 * @param array  $field
	 * @param int    $i
	 * @param string $name
	 * @param string $key
	 */
	protected function do_subfield_description( $theme_supports, $field, $i, $name, $key ) {
		$theme_support = sixtenpressblockeditor_get_theme_support_array( $theme_supports );
		if ( ! is_array( $theme_support ) ) {
			return;
		}
		$setting = get_option( $this->tab, array() );
		if ( empty( $setting[ $field['id'] ][ $i ] ) ) {
			return;
		}
		$theme_slugs = wp_list_pluck( $theme_support, $key, 'slug' );
		if ( ! array_key_exists( $setting[ $field['id'] ][ $i ]['slug'], $theme_slugs ) ) {
			return;
		}
		// Translators: the name of the setting (color or font) which has been defined by the theme
		$description = sprintf( __( 'This %s has been defined by your theme and will not be overridden by this setting.', 'sixtenpress-block-editor' ), $name );
		if ( ! in_array( $setting[ $field['id'] ][ $i ][ $key ], $theme_slugs, true ) ) {
			// Translators: 1. the name of the setting 2. the value of the setting
			$description .= ' ' . sprintf( __( 'The theme defined %1$s is %2$s.' ), $name, $theme_slugs[ $setting[ $field['id'] ][ $i ]['slug'] ] );
		}
		printf( '<p class="description">%s</p>', esc_html( $description ) );
	}

	/**
	 * Add a tab to the 6/10 Press settings page.
	 *
	 * @param $tabs
	 *
	 * @return array
	 */
	public function add_tab( $tabs ) {
		$tabs[] = array(
			'id'  => 'blockeditor',
			'tab' => __( 'Block Editor', 'sixtenpress-block-editor' ),
		);

		return $tabs;
	}

	/**
	 * Create a settings section if 6/10 press is not active.
	 * @return array
	 */
	public function define_section() {
		return include 'sections.php';
	}

	/**
	 * Check to see if block areas are enabled.
	 *
	 * @return boolean
	 */
	protected function are_block_areas_enabled() {
		return $this->get_option( 'enable_block_areas' );
	}

	/**
	 * Print the section description.
	 */
	public function blockeditor_section_description() {
		echo '<p>' . esc_html__( '6/10 Press Block Editor allows you to disable custom (freeform) colors and font sizes, and to define your own site color palette.', 'sixtenpress-block-editor' ) . '</p>';
	}

	/**
	 * Define the settings fields.
	 *
	 * @return array
	 */
	protected function register_fields() {
		$fields = include 'fields.php';
		if ( $this->are_block_areas_enabled() ) {
			$fields = array_merge( $fields, include 'fields-blockareas.php' );
		}

		return apply_filters( 'sixtenpressblockeditor_settings_fields', $fields );
	}

	/**
	 * If the font value is just a number, cast it to an integer.
	 *
	 * @param array $new_field_value
	 * @return array
	 * @since 0.9.1
	 */
	public function cast_font_to_integer( $new_field_value ) {
		foreach ( $new_field_value as $key => &$value ) {
			if ( empty( $value['size'] ) ) {
				continue;
			}
			if ( is_numeric( $value['size'] ) ) {
				$new_field_value[ $key ]['size'] = (int) $value['size'];
			}
		}

		return $new_field_value;
	}

	/**
	 * Get the site's public content types.
	 *
	 * @return array
	 */
	protected function get_content_types() {
		$args   = array(
			'public' => true,
		);
		$output = 'names';

		$post_types = get_post_types( $args, $output );
		$options    = array();
		foreach ( $post_types as $post_type ) {
			$object                = get_post_type_object( $post_type );
			$options[ $post_type ] = $object->label;
		}

		return $options;
	}
}
