<?php

$style    = '.sixten-meta, p.description { max-width: 800px; }';
$table    = '.sixtenpress-blockeditor-small-table';
$elements = array(
	''              => 'margin: 18px 0;',
	'th'            => 'padding: 5px; background: #fefefe;',
	'td'            => 'padding: 5px; background: #fff;',
	'.color-sample' => 'height: 16px; width: 16px; display: inline-block; border-radius: 8px; float: right; border: 1px solid #ddd;',
);
foreach ( $elements as $selector => $rule ) {
	$style .= $table . ' ' . $selector . '{' . $rule . '}';
}
$mobile = array(
	''          => 'width: 100%; max-width: 400px;',
	'th'        => 'display: none;',
	'td:before' => 'content: attr(data-label) \': \'; font-weight: 700;',
);
foreach ( $mobile as $selector => $rule ) {
	$style .= '@media screen and (max-width: 782px) { ' . $table . ' ' . $selector . ' { ' . $rule . ' } }';
}
$theme_support = sixtenpressblockeditor_get_theme_support_array( 'editor-color-palette' );
if ( is_array( $theme_support ) ) {
	foreach ( $theme_support as $color ) {
		$style .= '.color-sample--' . $color['slug'] . '{background-color:' . $color['color'] . ';}';
	}
}

return $style;
