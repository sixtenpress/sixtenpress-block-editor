<?php

$posts   = $this->get_post_list( 'block_area' );
$options = $posts;
$page    = apply_filters( 'sixtenpressblockeditor_onboarding_block_area', null );
if ( $page ) {
	unset( $options[ $page ] );
}
$fields = array(
	array(
		'id'              => 'block_areas',
		'title'           => __( 'Place Block Areas', 'sixtenpress-block-editor' ),
		'type'            => 'group',
		'section'         => 'blockeditor_areas',
		'active_callback' => array( $this, 'are_block_areas_enabled' ),
		'group'           => array(
			array(
				'id'      => 'post',
				'type'    => 'select',
				'options' => $options,
				'title'   => __( 'Block Area', 'sixtenpress-block-editor' ),
				'before'  => '<div class="one-half">',
				'after'   => '</div>',
				'class'   => 'widefat',
			),
			array(
				'id'     => 'hook',
				'type'   => 'text',
				'title'  => __( 'Hook (Block Area Location)', 'sixtenpress-block-editor' ),
				'before' => '<div class="one-half">',
				'after'  => '</div>',
			),
			array(
				'id'      => 'priority',
				'type'    => 'number',
				'title'   => __( 'Priority', 'sixtenpress-block-editor' ),
				'before'  => '<div class="one-half">',
				'after'   => '</div>',
				'default' => 10,
				'label'   => ' ' . __( 'Priority', 'sixtenpress-block-editor' ),
			),
			array(
				'id'      => 'show',
				'type'    => 'radio',
				'title'   => __( 'Show Block Area on:', 'sixtenpress-block-editor' ),
				'before'  => '<div class="one-half">',
				'after'   => '</div>',
				'options' => array(
					'all'      => __( 'Sitewide', 'sixtenpress-block-editor' ),
					'singular' => __( 'Singular Posts/Content', 'sixtenpress-block-editor' ),
					'archive'  => __( 'Archives', 'sixtenpress-block-editor' ),
				),
				'default' => 'all',
			),
			array(
				'id'          => 'post_type',
				'type'        => 'multiselect',
				'title'       => __( 'Limit Block Area to Specific Content Types:', 'sixtenpress-block-editor' ),
				'options'     => array( $this, 'get_content_types' ),
				'default'     => array(),
				'clear'       => false,
				'description' => __( 'Leave these empty to show the block area on all content types.', 'sixtenpress-block-editor' ),
			),
		),
		'repeatable'      => true,
	),
);
if ( apply_filters( 'sixtenpressblockeditor_onboarding_enable', false ) ) {
	$fields[] = array(
		'id'      => 'onboarding_label',
		'type'    => 'text',
		'title'   => __( 'Menu/Widget Label', 'sixtenpress-block-editor' ),
		'section' => 'blockeditor_onboarding',
	);
	$fields[] = array(
		'id'      => 'onboarding_post',
		'type'    => 'select',
		'options' => $posts,
		'title'   => __( 'Block Area', 'sixtenpress-block-editor' ),
		'class'   => 'widefat',
		'section' => 'blockeditor_onboarding',
	);
	$fields[] = array(
		'id'      => 'onboarding_dashboard',
		'type'    => 'wysiwyg',
		'title'   => __( 'Dashboard Widget Content', 'sixtenpress-block-editor' ),
		'section' => 'blockeditor_onboarding',
	);
}

return $fields;
