<?php

class SixTenPressSettingsBlockEditorCustomize {

	/**
	 * The setting key.
	 */
	private $setting = 'sixtenpressblockeditor';

	/**
	 * The panel to which to add our section/fields.
	 *
	 * @var string
	 */
	private $panel = 'sixtenpress';

	/**
	 * Define the section(s) to be added to the customizer.
	 *
	 * @param array $sections
	 * @return array
	 */
	public function add_sections( $sections ) {
		$sections[ $this->setting ] = array(
			'title'       => __( 'Block Editor', 'sixtenpress-block-editor' ),
			'priority'    => 15,
			'description' => '',
			'panel'       => $this->panel,
		);

		return $sections;
	}

	/**
	 * Define the fields to be added to the customizer.
	 *
	 * @param array $fields
	 * @return array
	 */
	public function add_fields( $fields ) {
		$fields[] = array(
			'id'          => 'disable_block_styles',
			'default'     => false,
			'type'        => 'checkbox',
			'title'       => __( 'Block Styles', 'sixtenpress-block-editor' ),
			'description' => __( 'Replace the default block editor styles with something a little less opinionated', 'sixtenpress-block-editor' ),
			'setting'     => $this->setting,
			'section'     => "{$this->panel}_{$this->setting}",
		);

		return $fields;
	}
}
