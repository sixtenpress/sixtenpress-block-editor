<?php

return apply_filters(
	'sixtenpressblockeditor_settings_defaults',
	array(
		'disable_block_styles'     => 1,
		'disable_custom_colors'    => 0,
		'disable_font_sizes'       => 0,
		'disable_custom_gradients' => 0,
		'colors'                   => array(),
		'fonts'                    => array(),
		'align_wide'               => 0,
		'responsive_embeds'        => 0,
		'enable_block_areas'       => 1,
		'block_areas'              => array(),
		'current_theme_support'    => array(),
		'onboarding_post'          => null,
		'onboarding_label'         => __( 'Client Portal', 'sixtenpress-block-editor' ),
		'onboarding_dashboard'     => __( 'Need help getting started? Visit the "Getting Started" page.', 'sixtenpress-block-editor' ),
	)
);
