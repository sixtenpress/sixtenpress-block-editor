<?php

$fields = array(
	array(
		'id'      => 'disable_block_styles',
		'title'   => __( 'Block Styles', 'sixtenpress-block-editor' ),
		'label'   => __( 'Replace the default block editor styles with something a little less opinionated', 'sixtenpress-block-editor' ),
		'type'    => 'checkbox',
		'section' => 'blockeditor',
	),
	array(
		'id'      => 'disable_custom_colors',
		'title'   => __( 'Disable Custom Colors', 'sixtenpress-block-editor' ),
		'label'   => __( 'Restrict block editor color choices to what\'s defined by your theme (or on this settings page)', 'sixtenpress-block-editor' ),
		'type'    => 'checkbox',
		'section' => 'blockeditor',
	),
	array(
		'id'      => 'disable_font_sizes',
		'title'   => __( 'Disable Custom Font Sizes', 'sixtenpress-block-editor' ),
		'label'   => __( 'Restrict block editor font sizes to what\'s defined by your theme (or on this settings page)', 'sixtenpress-block-editor' ),
		'type'    => 'checkbox',
		'section' => 'blockeditor',
	),
	array(
		'id'      => 'disable_custom_gradients',
		'title'   => __( 'Disable Custom Gradients', 'sixtenpress-block-editor' ),
		'label'   => __( 'Restrict block editor gradients to what\'s defined by your theme', 'sixtenpress-block-editor' ),
		'type'    => 'checkbox',
		'section' => 'blockeditor',
	),
	array(
		'id'      => 'enable_block_areas',
		'title'   => __( 'Enable Block Areas', 'sixtenpress-block-editor' ),
		'label'   => __( 'Create Block Areas content type, which can be used instead of widgets', 'sixtenpress-block-editor' ),
		'type'    => 'checkbox',
		'section' => 'blockeditor_areas',
	),
	array(
		'id'          => 'colors',
		'title'       => __( 'Color Palette', 'sixtenpress-block-editor' ),
		'type'        => 'group',
		'section'     => 'blockeditor_custom',
		'group'       => array(
			array(
				'id'     => 'name',
				'type'   => 'text',
				'title'  => __( 'Color Name', 'sixtenpress-block-editor' ),
				'before' => '<div class="one-third">',
				'after'  => '</div>',
			),
			array(
				'id'          => 'slug',
				'type'        => 'text',
				'title'       => __( 'Color Slug', 'sixtenpress-block-editor' ),
				'before'      => '<div class="one-third">',
				'after'       => '</div>',
				'description' => __( 'Required', 'sixtenpress-block-editor' ),
			),
			array(
				'id'     => 'color',
				'type'   => 'color',
				'title'  => __( 'Hex Code', 'sixtenpress-block-editor' ),
				'before' => '<div class="one-third">',
				'after'  => '</div>',
			),
		),
		'repeatable'  => true,
		'description' => array( $this, 'colors_field_description' ),
	),
	array(
		'id'          => 'fonts',
		'title'       => __( 'Fonts', 'sixtenpress-block-editor' ),
		'type'        => 'group',
		'section'     => 'blockeditor_custom',
		'group'       => array(
			array(
				'id'     => 'name',
				'type'   => 'text',
				'title'  => __( 'Font Size Name', 'sixtenpress-block-editor' ),
				'before' => '<div class="one-third">',
				'after'  => '</div>',
			),
			array(
				'id'          => 'slug',
				'type'        => 'text',
				'title'       => __( 'Slug', 'sixtenpress-block-editor' ),
				'before'      => '<div class="one-third">',
				'after'       => '</div>',
				'description' => __( 'Required', 'sixtenpress-block-editor' ),
			),
			array(
				'id'          => 'size',
				'type'        => 'text',
				'title'       => __( 'Size', 'sixtenpress-block-editor' ),
				'before'      => '<div class="one-third">',
				'after'       => '</div>',
				'description' => __( 'For px, use a number only for the value.', 'sixtenpress-block-editor' ),
			),
		),
		'repeatable'  => true,
		'description' => array( $this, 'fonts_field_description' ),
	),
);

if ( ! get_theme_support( 'align-wide' ) ) {
	$fields[] = array(
		'id'      => 'align_wide',
		'title'   => __( 'Wide/Full Block Support', 'sixtenpress-block-editor' ),
		'label'   => __( 'Add support for wide/full width blocks', 'sixtenpress-block-editor' ),
		'type'    => 'checkbox',
		'section' => 'blockeditor',
	);
}
if ( ! get_theme_support( 'responsive-embeds' ) ) {
	$fields[] = array(
		'id'      => 'responsive_embeds',
		'title'   => __( 'Responsive Embeds', 'sixtenpress-block-editor' ),
		'label'   => __( 'Make embeds (such as videos) responsive', 'sixtenpress-block-editor' ),
		'type'    => 'checkbox',
		'section' => 'blockeditor',
	);
}

return $fields;
