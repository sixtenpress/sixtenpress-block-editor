<?php

/**
 * Class SixTenPressBlockEditorSettingsEnqueue
 */
class SixTenPressBlockEditorSettingsEnqueue {

	/**
	 * SixTenPressBlockEditorSettingsEnqueue constructor.
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_post_meta' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_post_meta_style' ) );
	}

	/**
	 * Enqueue the custom style for the post meta fields.
	 */
	public function enqueue_post_meta_style() {
		$go = apply_filters( 'sixtenpress_admin_style', false );
		if ( ! did_action( 'sixtenpress_fields_init' ) && ! $go ) {
			return;
		}
		wp_enqueue_style( 'sixtenpress-postmeta', esc_url( plugins_url( '/css/sixtenpress-postmeta.css', dirname( __FILE__ ) ) ), array(), SIXTENPRESSBLOCKEDITOR_VERSION, 'screen' );
	}

	/**
	 * Enqueue the post meta script.
	 */
	public function enqueue_post_meta() {
		$go = apply_filters( 'sixtenpress_admin_post_meta', false );
		if ( ! $go ) {
			return;
		}
		wp_enqueue_script(
			'sixtenpress-post-meta',
			plugins_url( "/js/post-meta{$this->minify()}.js", dirname( __FILE__ ) ),
			array(
				'jquery',
				'jquery-ui-sortable',
			),
			SIXTENPRESSBLOCKEDITOR_VERSION,
			true
		);
		wp_localize_script( 'sixtenpress-post-meta', 'SixTenRepeaters', $this->get_post_meta_localization() );
	}

	/**
	 * Get the localization args for the post meta script.
	 *
	 * @return array
	 */
	protected function get_post_meta_localization() {
		$labels = $this->get_post_meta_labels();
		return array(
			'confirm'      => $labels['confirm'],
			'color_picker' => false,
			'buttons'      => array(
				'add'    => array(
					'icon'  => 'dashicons-plus-alt',
					'class' => 'add-group',
					'label' => $labels['add'],
				),
				'remove' => array(
					'icon'  => 'dashicons-dismiss',
					'class' => 'remove-group',
					'label' => $labels['remove'],
				),
				'up'     => array(
					'icon'  => 'dashicons-arrow-up-alt2',
					'class' => 'shift shift-up',
					'label' => $labels['up'],
				),
				'down'   => array(
					'icon'  => 'dashicons-arrow-down-alt2',
					'class' => 'shift shift-down',
					'label' => $labels['down'],
				),
			),
			'toggle'       => array(
				'icon'  => 'dashicons-admin-generic',
				'class' => 'toggle',
				'label' => $labels['toggle'],
			),
			'move'         => array(
				'icon'  => 'dashicons-move',
				'class' => 'move-group',
				'label' => $labels['move'],
			),
		);
	}

	/**
	 * Set the labels for buttons, confirmations, etc. for post meta.
	 * @return array
	 */
	protected function get_post_meta_labels() {
		return apply_filters(
			'sixtenpress_post_meta_labels',
			array(
				'confirm' => __( 'Are you sure you want to delete this row?', 'sixtenpress' ),
				'add'     => __( 'Add Row', 'sixtenpress' ),
				'remove'  => __( 'Remove Row', 'sixtenpress' ),
				'up'      => __( 'Shift Row Up', 'sixtenpress' ),
				'down'    => __( 'Shift Row Down', 'sixtenpress' ),
				'toggle'  => __( 'Section Settings', 'sixtenpress' ),
				'move'    => __( 'Move Row', 'sixtenpress' ),
			)
		);
	}

	/**
	 * Get minified file names.
	 *
	 * @return string
	 */
	protected function minify() {
		return defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	}
}
