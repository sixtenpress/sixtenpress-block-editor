<?php

$sections = array(
	'blockeditor'        => array(
		'id'    => 'blockeditor',
		'tab'   => 'blockeditor',
		'title' => __( 'Block Editor Settings', 'sixtenpress-block-editor' ),
	),
	'blockeditor_areas'  => array(
		'id'    => 'blockeditor_areas',
		'tab'   => 'blockeditor',
		'title' => __( 'Block Areas', 'sixtenpress-block-editor' ),
	),
	'blockeditor_custom' => array(
		'id'    => 'blockeditor_custom',
		'tab'   => 'blockeditor',
		'title' => __( 'Block Editor Custom Settings', 'sixtenpress-block-editor' ),
	),
);

if ( $this->are_block_areas_enabled() && apply_filters( 'sixtenpressblockeditor_onboarding_enable', false ) ) {
	$sections['blockeditor_onboarding'] = array(
		'id'    => 'blockeditor_onboarding',
		'tab'   => 'blockeditor',
		'title' => __( 'Welcome/Client Portal', 'sixtenpress-block-editor' ),
	);
}

return $sections;
