<?php

return array(
	'heightLabel'   => __( 'Block Height', 'sixtenpress-block-editor' ),
	'heightOptions' => array(
		array(
			'label' => __( 'None', 'sixtenpress-block-editor' ),
			'value' => '',
		),
		array(
			'label' => __( 'Short', 'sixtenpress-block-editor' ),
			'value' => 'sm',
		),
		array(
			'label' => __( 'Medium', 'sixtenpress-block-editor' ),
			'value' => 'md',
		),
		array(
			'label' => __( 'Tall', 'sixtenpress-block-editor' ),
			'value' => 'lg',
		),
		array(
			'label' => __( 'Extra Tall', 'sixtenpress-block-editor' ),
			'value' => 'xl',
		),
	),
	'heightBlocks'  => array( 'core/columns', 'core/cover', 'core/group' ),
	'responsive'    => array(
		'label' => __( 'Make columns not responsive', 'sixtenpress-block-editor' ),
		'on'    => __( 'Will not stack on mobile', 'sixtenpress-block-editor' ),
		'off'   => __( 'Will stack on mobile', 'sixtenpress-block-editor' ),
	),
	'gallery'       => array(
		'label' => __( 'Adjust gallery margin width', 'sixtenpress-block-editor' ),
	),
	'widthLabel'    => __( 'Maximum Block Width', 'sixtenpress-block-editor' ),
	'widthOptions'  => array(
		array(
			'label' => __( 'None (Full Width)', 'sixtenpress-block-editor' ),
			'value' => '',
		),
		array(
			'label' => __( 'Narrow', 'sixtenpress-block-editor' ),
			'value' => 'sm',
		),
		array(
			'label' => __( 'Medium', 'sixtenpress-block-editor' ),
			'value' => 'md',
		),
		array(
			'label' => __( 'Wide', 'sixtenpress-block-editor' ),
			'value' => 'lg',
		),
		array(
			'label' => __( 'Extra Wide', 'sixtenpress-block-editor' ),
			'value' => 'xl',
		),
	),
	'widthBlocks'  => array( 'core/cover', 'core/group' ),
);
