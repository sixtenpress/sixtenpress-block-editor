<?php

return <<<CSS
.alignwide, .alignfull {
	margin: 0 auto;
}

.full-width-content .alignfull,
.full-width-content .alignwide {
	max-width: 100vw;
}

.full-width-content .alignfull {
	margin-left: calc(50% - 50vw);
	margin-right: calc(50% - 50vw);
	width: 100vw;
}

.full-width-content .alignwide {
	margin-left: calc(25% - 25vw);
	margin-right: calc(25% - 25vw);
	width: calc(50% + 50vw);
}

img.alignwide,
img.alignfull {
	margin-bottom: 24px;
}

@media only screen and (min-width: 768px) {
	.alignfull, .alignwide {
		margin: 0 -40px;
		max-width: calc(100% + 80px);
		width: calc(100% + 80px);
	}
}
CSS;
