<?php

$css = '';
foreach ( $setting['fonts'] as $font ) {
	if ( ! $font['slug'] ) {
		continue;
	}
	$size = is_numeric( $font['size'] ) ? "{$font['size']}px" : $font['size'];
	$css .= sprintf(
		'.has-%1$s-font-size { font-size: %2$s; } ',
		$font['slug'],
		$size
	);
}
if ( ! $css && ! sixtenpressblockeditor_get_theme_support_array( 'editor-font-sizes' ) ) {
	$sizes = array(
		'small'  => .9,
		'normal' => 1,
		'medium' => 1.5,
		'large'  => 1.75,
		'huge'   => 2.5,
	);
	foreach ( $sizes as $key => $value ) {
		$css .= sprintf(
			'.has-%1$s-font-size { font-size: %2$sem; } ',
			$key,
			$value
		);
	}
}

return $css;
