<?php

$css = '';
foreach ( $setting['colors'] as $color ) {
	if ( ! $color['slug'] ) {
		continue;
	}
	$css .= sprintf(
		'.has-%1$s-background-color { background-color: %2$s; }
		 .has-%1$s-color { color: %2$s; }
		 .has-%1$s-color a { color: %3$s; } ',
		$color['slug'],
		$color['color'],
		$this->color_brightness( $color['color'], -18 )
	);
}

if ( ! $css && ! sixtenpressblockeditor_get_theme_support_array() ) {
	$colors = array(
		'pale-pink'             => '#f78da7',
		'vivid-red'             => '#cf2e2e',
		'luminous-vivid-orange' => '#ff6900',
		'luminous-vivid-amber'  => '#fcb900',
		'light-green-cyan'      => '#7bdcb5',
		'vivid-green-cyan'      => '#00d084',
		'pale-cyan-blue'        => '#8ed1fc',
		'vivid-cyan-blue'       => '#0693e3',
		'vivid-purple'          => '#9b51e0',
		'very-light-gray'       => '#eee',
		'cyan-bluish-gray'      => '#abb8c3',
		'very-dark-gray'        => '#313131',
	);

	foreach ( $colors as $slug => $hex ) {
		$css .= sprintf(
			'.has-%1$s-background-color { background-color: %2$s; }
			 .has-%1$s-color { color: %2$s; } ',
			$slug,
			$hex
		);
	}
}

return $css;
