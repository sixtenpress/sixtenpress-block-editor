<?php

$css = <<<CSS
.sixtenpress-info {
	background-color: white;
	box-sizing: border-box;
	margin: 20px 20px 20px 0;
	max-width: 800px;
	padding: 36px;
}

.sixtenpress-info p {
	font-size: 16px;
}

.sixtenpress-info iframe {
	display: block;
	margin: 0 auto;
}
CSS;

return $css;
