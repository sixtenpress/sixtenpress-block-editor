<?php

/**
 * The onboarding class to allow a developer to leave a message for the end user.
 *
 * @since 2.7.0
 */
class SixTenPressBlockEditorOnboarding {

	/**
	 * The block area to be used for the onboarding content.
	 *
	 * @var integer
	 */
	private $page;

	/**
	 * The label for the admin menu.
	 *
	 * @var string
	 */
	private $label;

	/**
	 * The top level admin page.
	 *
	 * @var string
	 */
	private $admin_page = 'tools.php';

	/**
	 * The slug for the submenu page.
	 *
	 * @var string
	 */
	private $slug = 'sixtenpress-info';

	/**
	 * The onboarding class constructor.
	 */
	public function __construct( $page ) {
		$this->page  = $page;
		$this->label = sixtenpressblockeditor_get_setting( 'onboarding_label' );
	}

	/**
	 * Add the submenu page if the onboarding block area is published.
	 */
	public function add_submenu_page() {
		if ( ! $this->does_onboarding_page_exist() ) {
			return;
		}

		add_action( 'wp_dashboard_setup', array( $this, 'add_dashboard_widget' ) );
		add_submenu_page(
			$this->admin_page,
			$this->label,
			$this->label,
			'manage_options',
			$this->slug,
			array( $this, 'onboarding_content' ),
			3
		);
	}

	/**
	 * Output the content for the onboarding block area.
	 */
	public function onboarding_content() {
		$post = $this->get_post_object();
		wp_add_inline_style( 'wp-block-library', $this->inline_style() );
		wp_print_styles( 'wp-block-library' );
		echo '<div class="' . esc_attr( $this->slug ) . '">';
		echo '<h1>' . esc_attr( $post->post_title ) . '</h1>';
		$content = apply_filters( 'the_content', wp_kses_post( $post->post_content ) );
		echo $content;
		echo '</div>';
	}

	/**
	 * Add an inline style for the onboarding page.
	 * Added to the block library style.
	 *
	 * @return string
	 */
	private function inline_style() {
		return apply_filters( 'sixtenpressblockeditor_onboarding_style', include 'css/css-onboarding.php' );
	}

	/**
	 * Add a small dashboard widget to remind users that the getting started page exists.
	 */
	public function add_dashboard_widget() {
		if ( ! current_user_can( 'manage_options' ) || ! $this->get_dashboard_widget_content() ) {
			return;
		}
		wp_add_dashboard_widget(
			'sixtenpressonboarding',
			$this->label,
			array( $this, 'dashboard_widget_content' )
		);
	}

	/**
	 * Output the dashboard widget content.
	 */
	public function dashboard_widget_content() {
		echo wp_kses_post( $this->get_dashboard_widget_content() );
		$link = add_query_arg(
			array(
				'page' => $this->slug,
			),
			admin_url( $this->admin_page )
		);
		printf(
			'<p><a class="button button-primary" href="%s">%s</a></p>',
			esc_url( $link ),
			esc_attr( $this->label )
		);
	}

	/**
	 * Get the dashboard widget content.
	 *
	 * @return void
	 */
	private function get_dashboard_widget_content() {
		return sixtenpressblockeditor_get_setting( 'onboarding_dashboard' );
	}

	/**
	 * Check to see if the onboarding block area exists and is published.
	 *
	 * @return boolean
	 */
	private function does_onboarding_page_exist() {
		if ( ! post_type_exists( 'block_area' ) ) {
			return false;
		}
		$object = $this->get_post_object();

		return is_object( $object ) && 'publish' === $object->post_status;
	}

	/**
	 * Get the block area post object.
	 *
	 * @param string|int $post
	 * @return object
	 */
	private function get_post_object( $post = '' ) {
		if ( ! $post ) {
			$post = $this->page;
		}
		if ( is_string( $post ) ) {
			return get_page_by_path( $post, OBJECT, 'block_area' );
		}

		return get_post( $post );
	}
}
