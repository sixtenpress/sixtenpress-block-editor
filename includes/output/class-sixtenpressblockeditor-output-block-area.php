<?php

/**
 * Block Area Output
 * CPT for block-based widget areas, until WP core adds block support to widget areas
 * @see          https://www.billerickson.net/wordpress-gutenberg-block-widget-areas/
 *
 * @package      SixTenPressBlockEditor
 * @author       Robin Cornett
 * @author       Bill Erickson
 * @since        0.4.0
 * @license      GPL-2.0+
 **/
class SixTenPressBlockEditorOutputBlockArea {

	/**
	 * Instance of the class.
	 * @var object
	 */
	private static $instance;

	/**
	 * @var string $post_type
	 */
	protected $post_type = 'block_area';

	/**
	 * @var string $meta_key
	 */
	private $meta_key = '_sixtenpressblockeditor';

	/**
	 * Class Instance.
	 * @return SixTenPressBlockEditorOutputBlockArea
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof SixTenPressBlockEditorOutputBlockArea ) ) {
			self::$instance = new SixTenPressBlockEditorOutputBlockArea();

			add_action( 'wp_head', array( self::$instance, 'do_block_areas' ) );
		}

		return self::$instance;
	}

	/**
	 * Automatically add block areas created using the CPT.
	 */
	public function do_block_areas() {
		if ( ! $this->do_block_areas_exist() ) {
			return;
		}
		$setting = sixtenpressblockeditor_get_setting( 'block_areas' );
		if ( ! $setting ) {
			return;
		}
		foreach ( $setting as $block_area ) {
			$block_area = $this->update_block_area_defaults( $block_area );
			if ( empty( $block_area['post'] ) || empty( $block_area['hook'] ) ) {
				continue;
			}
			if ( ! $this->can_show_block_area( $block_area ) ) {
				continue;
			}
			add_action(
				esc_attr( $block_area['hook'] ),
				function () use ( $block_area ) {
					$this->show( $block_area['post'] );
				},
				$block_area['priority']
			);
		}
	}

	/**
	 * Parse the block area with defaults.
	 *
	 * @param $block_area
	 *
	 * @return array
	 */
	private function update_block_area_defaults( $block_area ) {
		$defaults = array(
			'post'      => '',
			'text'      => '',
			'priority'  => 10,
			'show'      => 'all',
			'post_type' => array(),
		);

		return wp_parse_args( $block_area, $defaults );
	}

	/**
	 * Check to see if the block area can be shown.
	 * Currently only supports singular (general) and archives, or all.
	 *
	 * @param array $block_area
	 * @return boolean
	 * @since 0.4.0
	 *
	 */
	private function can_show_block_area( $block_area ) {
		$can_show = true;
		if ( 'singular' === $block_area['show'] && ! is_singular() ) {
			$can_show = false;
		} elseif ( 'archive' === $block_area['show'] && is_singular() ) {
			$can_show = false;
		}
		if ( $can_show && ! empty( $block_area['post_type'] ) ) {
			$post_type = get_post_type();
			if ( ! in_array( $post_type, $block_area['post_type'], true ) || is_search() ) {
				$can_show = false;
			}
		}
		if ( $this->is_landing_page() ) {
			$can_show = false;
		}

		/**
		 * Add a filter on the block area conditional so that it can be modified.
		 *
		 * @param boolean $can_show
		 * @param array   $block_area
		 */
		return (bool) apply_filters( 'sixtenpressblockeditor_can_show_block_area', $can_show, $block_area );
	}

	/**
	 * Determine if the current page template is a landing page.
	 * Checks against most common Genesis landing page template slug.
	 *
	 * @return boolean
	 */
	private function is_landing_page() {
		$templates = apply_filters(
			'sixtenpressblockeditor_landing_page_templates',
			array(
				'page_landing.php',
				'templates/page_landing.php',
				'templates/landing.php',
				'page-templates/landing.php',
			)
		);

		return is_page_template( $templates );
	}

	/**
	 * Show block area
	 *
	 * @param string|int $post
	 * @param array      $args
	 */
	public function show( $post = '', $args = array() ) {
		if ( ! $this->do_block_areas_exist() ) {
			return;
		}
		if ( ! $post ) {
			return;
		}
		global $wp_query;
		$loop = new WP_Query( $this->get_query_args( $post ) );
		// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
		$wp_query = $loop;
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) {
				$loop->the_post();
				$args = $this->update_args( get_the_ID(), $args );
				if ( $args['echo'] ) {
					$this->echo_block_area( $post, $args );
				} else {
					$output = $this->get( $post, $args );
					$this->reset();
					return $output;
				}
			}
		}
		$this->reset();
	}

	/**
	 * Echo the block area.
	 *
	 * @param string|int $post
	 * @param array      $args
	 * @since 0.8.0
	 */
	private function echo_block_area( $post, $args ) {
		printf(
			'<div class="%s"%s>',
			esc_attr( implode( ' ', $this->get_block_area_classes( $post, $args ) ) ),
			wp_kses_post( $this->get_style( $args ) )
		);
		if ( ! empty( $args['wrap'] ) ) {
			echo '<div class="wrap">';
		}
		the_content();
		if ( ! empty( $args['wrap'] ) ) {
			echo '</div>';
		}
		echo '</div>';
	}

	/**
	 * Get the block area, instead of echoing it.
	 *
	 * @param  string|int $post
	 * @param  array      $args
	 * @return string
	 *
	 * @since 0.8.0
	 */
	private function get( $post, $args ) {
		$content = get_the_content();
		if ( ! $content ) {
			return '';
		}
		$output = sprintf(
			'<div class="%s"%s>',
			esc_attr( implode( ' ', $this->get_block_area_classes( $post, $args ) ) ),
			wp_kses_post( $this->get_style( $args ) )
		);
		if ( ! empty( $args['wrap'] ) ) {
			$output .= sprintf( '<div class="wrap">%s</div>', $content );
		} else {
			$output .= $content;
		}
		$output .= '</div>';
		wp_reset_postdata();

		return do_blocks( $output );
	}

	/**
	 * Reset the global query.
	 */
	private function reset() {
		wp_reset_postdata();
		// phpcs:ignore WordPress.WP.DiscouragedFunctions.wp_reset_query_wp_reset_query
		wp_reset_query();
	}

	/**
	 * Get the query args to display the block area.
	 *
	 * @param $post
	 *
	 * @return array
	 */
	private function get_query_args( $post ) {
		$query_args = array(
			'post_type'      => $this->post_type,
			'posts_per_page' => 1,
			'post_status'    => 'publish',
		);
		if ( is_numeric( $post ) ) {
			$query_args['p'] = $post;
		} else {
			$query_args['name'] = $post;
		}

		return $query_args;
	}

	/**
	 * Parse the post meta with custom args.
	 *
	 * @param $post_id
	 * @param $args
	 *
	 * @return array
	 * @since 0.4.0
	 */
	private function update_args( $post_id, $args ) {
		$defaults = array(
			'wrap'       => 0,
			'class'      => '',
			'background' => '',
			'color'      => '',
			'echo'       => true,
		);
		$args     = wp_parse_args( $args, $defaults );
		$meta     = get_post_meta( $post_id, $this->meta_key, true );
		if ( $meta ) {
			foreach ( $meta as $key => $value ) {
				if ( $value ) {
					$args[ $key ] = $value;
				}
			}
		}

		return $args;
	}

	/**
	 * Get the inline style for the block.
	 * @since 0.4.0
	 *
	 * @param $args
	 * @return string
	 */
	private function get_style( $args ) {
		$style = '';
		if ( empty( $args['background'] ) && ! empty( $args['background_hex'] ) ) {
			$style .= "background-color:{$args['background_hex']};";
		}
		if ( empty( $args['color'] ) && ! empty( $args['color_hex'] ) ) {
			$style .= "color:{$args['color_hex']};";
		}

		return $style ? ' style="' . $style . '"' : $style;
	}

	/**
	 * Get the classes for the block area.
	 *
	 * @param $post
	 * @param $args
	 *
	 * @return array
	 */
	private function get_block_area_classes( $post, $args ) {
		if ( is_numeric( $post ) ) {
			$post = get_post_field( 'post_name', $post );
		}
		$classes = array( 'block-area', "block-area-{$post}" );
		if ( ! empty( $args['class'] ) ) {
			$classes[] = $args['class'];
		}
		if ( ! empty( $args['background_hex'] ) ) {
			$classes[] = 'has-background';
		}
		if ( ! empty( $args['background'] ) ) {
			$classes[] = "has-{$args['background']}-background-color";
		}
		if ( ! empty( $args['color_hex'] ) ) {
			$classes[] = 'has-text-color';
		}
		if ( ! empty( $args['color'] ) ) {
			$classes[] = "has-{$args['color']}-color";
		}

		return $classes;
	}

	/**
	 * Check to see if block areas are a registered post type.
	 * @return bool
	 */
	private function do_block_areas_exist() {
		return post_type_exists( $this->post_type );
	}
}

/**
 * The function provides access to the class methods.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * @return object
 */
function sixtenpressblockeditor_area() {
	return SixTenPressBlockEditorOutputBlockArea::instance();
}

sixtenpressblockeditor_area();
