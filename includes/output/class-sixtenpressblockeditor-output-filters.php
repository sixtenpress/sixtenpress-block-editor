<?php

/**
 * This class handles filters on rendered blocks.
 *
 * \SixTenPressBlockEditorOutputFilters
 */
class SixTenPressBlockEditorOutputFilters {

	/**
	 * Customize the gallery margin.
	 *
	 * @param string $block_content
	 * @param array $block
	 * @return string
	 * @since 0.9.0
	 */
	public function gallery( $block_content, $block ) {
		$setting = sixtenpressblockeditor_get_setting( 'disable_block_styles' );
		if ( ! $setting ) {
			return $block_content;
		}
		if ( 'core/gallery' !== $block['blockName'] ) {
			return $block_content;
		}
		if ( ! isset( $block['attrs']['galleryMargin'] ) || 4 === $block['attrs']['galleryMargin'] ) {
			return $block_content;
		}
		$style = '<style>.wp-block-gallery { --block-gallery-grid-size: ' . esc_attr( $block['attrs']['galleryMargin'] ) . 'px; }</style>';

		return $style . $block_content;
	}
}
