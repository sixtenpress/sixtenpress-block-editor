<?php

class SixTenPressBlockEditorOutputEnqueue {

	/**
	 * Enqueue the block editor styles.
	 */
	public function block_editor_styles() {
		if ( ! $this->is_custom_style_enabled() ) {
			return;
		}
		$this->block_library();
		wp_enqueue_style( 'sixtenpress-block-library-theme', plugins_url( 'css/blocks-editor.css', dirname( __FILE__ ) ), false, SIXTENPRESSBLOCKEDITOR_VERSION, 'all' );
	}

	/**
	 * Enqueue the custom wp-block-library style.
	 */
	public function block_library() {
		if ( ! $this->is_custom_style_enabled() ) {
			return;
		}
		wp_dequeue_style( 'wp-block-library' );
		wp_deregister_style( 'wp-block-library' );
		wp_enqueue_style( 'wp-block-library', plugins_url( 'css/blocks.css', dirname( __FILE__ ) ), false, SIXTENPRESSBLOCKEDITOR_VERSION, 'all' );
	}

	/**
	 * Gutenberg scripts and styles
	 * @see https://www.billerickson.net/wordpress-color-palette-button-styling-gutenberg
	 */
	public function block_editor_scripts() {
		$minify = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_enqueue_script(
			'sixtenpress-block-editor',
			plugins_url( "js/block-editor{$minify}.js", dirname( __FILE__ ) ),
			array(
				'jquery',
				'wp-blocks',
				'wp-dom',
			),
			SIXTENPRESSBLOCKEDITOR_VERSION,
			true
		);
		wp_localize_script(
			'sixtenpress-block-editor',
			'SixTenPressBlockEditorConfig',
			$this->get_localization_data()
		);
	}

	/**
	 * Add a script to add custom attribute(s) to certain blocks.
	 *
	 * @since 0.8.0
	 */
	public function block_editor_attributes() {
		if ( ! $this->is_custom_style_enabled() ) {
			return;
		}
		wp_enqueue_script(
			'sixtenpress-block-editor-attributes',
			plugins_url( 'build/index.js', dirname( dirname( __FILE__ ) ) ),
			array(
				'jquery',
				'wp-blocks',
				'wp-element',
				'wp-editor',
			),
			SIXTENPRESSBLOCKEDITOR_VERSION,
			true
		);
		wp_localize_script(
			'sixtenpress-block-editor-attributes',
			'SixTenPressBlockEditorAttributes',
			include 'custom-attributes.php'
		);
	}

	/**
	 * Get the localization data for the editor script.
	 *
	 * @return array
	 * @since 0.8.0
	 */
	private function get_localization_data() {
		return array(
			'colors'        => sixtenpressblockeditor_get_theme_support_array(),
			'disallow'      => $this->get_disallowed_blocks(),
			'remove_styles' => $this->get_removed_styles(),
			'new_styles'    => $this->get_new_styles(),
		);
	}

	/**
	 * Optionally unregister blocks registered with PHP.
	 *
	 * @since 0.8.0
	 */
	public function maybe_unregister_blocks() {
		$blocks = $this->get_disallowed_blocks();
		if ( ! $blocks ) {
			return;
		}
		foreach ( $blocks as $block_name ) {
			if ( ! WP_Block_Type_Registry::get_instance()->is_registered( $block_name ) ) {
				continue;
			}
			WP_Block_Type_Registry::get_instance()->unregister( $block_name );
		}
	}

	/**
	 * Get the list of disallowed blocks.
	 * Defined by users/developers, as JS registered blocks are not available.
	 *
	 * @return array
	 * @since 0.8.0
	 */
	private function get_disallowed_blocks() {
		$blocks = array();
		if ( function_exists( 'get_current_screen' ) ) {
			$screen = get_current_screen();
			if ( is_object( $screen ) && 'block_area' === $screen->post_type ) {
				$blocks = array(
					'core/more',
					'core/nextpage',
				);
			}
		}

		return apply_filters( 'sixtenpressblockeditor_disable_blocks', $blocks );
	}

	/**
	 * Get the styles to be removed from the editor.
	 *
	 * @return array
	 * @since 0.8.0
	 */
	private function get_removed_styles() {
		$styles = array();
		if ( $this->is_custom_style_enabled() ) {
			$styles = array(
				array(
					'block' => 'core/button',
					'name'  => 'squared',
				),
			);
		}

		return apply_filters( 'sixtenpressblockeditor_remove_block_editor_styles', $styles );
	}

	/**
	 * Get the styles to be added to the editor.
	 *
	 * @return array
	 * @since 0.8.0
	 */
	private function get_new_styles() {
		$styles = array();
		if ( $this->is_custom_style_enabled() ) {
			$styles = array(
				array(
					'block' => 'core/button',
					'name'  => 'wide',
					'label' => __( 'Full Width', 'sixtenpress-block-editor' ),
				),
				array(
					'block' => 'core/button',
					'name'  => 'rounded',
					'label' => __( 'Rounded', 'sixtenpress-block-editor' ),
				),
			);
		}

		return apply_filters( 'sixtenpressblockeditor_add_block_editor_styles', $styles );
	}

	/**
	 * Check to see if the plugin style is being used instead of the default
	 * Core style.
	 *
	 * @return boolean
	 * @since 0.8.0
	 */
	private function is_custom_style_enabled() {
		return (bool) sixtenpressblockeditor_get_setting( 'disable_block_styles' );
	}

	/**
	 * Add the custom color palette to the block library styles.
	 */
	public function inline_style() {
		$style   = '';
		$setting = sixtenpressblockeditor_get_setting();
		$style  .= include 'css/css-colors.php';
		$style  .= include 'css/css-fonts.php';
		$style  .= include 'css/css-gradients.php';
		if ( ! is_admin() ) {
			$style .= $this->get_wide_alignment_style( $setting );
		}

		wp_add_inline_style( 'wp-block-library', $this->minify( $style ) );
	}

	/**
	 * For themes which didn't support wide/full alignment, if we are adding
	 * support, add the necessary CSS.
	 *
	 * @param $setting
	 *
	 * @return string
	 */
	private function get_wide_alignment_style( $setting ) {
		if ( ! empty( $setting['current_theme_support']['align_wide'] ) ) {
			return '';
		}
		if ( empty( $setting['align_wide'] ) ) {
			return '';
		}

		return include 'css/css-alignwide.php';
	}

	/**
	 * Minify, somewhat, inline CSS.
	 *
	 * @param $css
	 * @return string
	 */
	private function minify( $css ) {
		$css = preg_replace( '/\s+/', ' ', $css );
		$css = preg_replace( '/(\s+)(\/\*(.*?)\*\/)(\s+)/', '$2', $css );
		$css = preg_replace( '~/\*(?![\!|\*])(.*?)\*/~', '', $css );
		$css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );
		$css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );
		$css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );
		$css = preg_replace( '/0 0 0 0/', '0', $css );
		$css = preg_replace( '/#([a-f0-9])\\1([a-f0-9])\\2([a-f0-9])\\3/i', '#\1\2\3', $css );

		return trim( $css );
	}

	/**
	 * Generates a lighter or darker color from a starting color.
	 * Used to generate complementary hover tints from user-chosen colors.
	 *
	 * @since 2.2.3
	 *
	 * @param string $color A color in hex format.
	 * @param int    $change The amount to reduce or increase brightness by.
	 * @return string Hex code for the adjusted color brightness.
	 */
	private function color_brightness( $color, $change ) {

		$hexcolor = str_replace( '#', '', $color );

		$red   = hexdec( substr( $hexcolor, 0, 2 ) );
		$green = hexdec( substr( $hexcolor, 2, 2 ) );
		$blue  = hexdec( substr( $hexcolor, 4, 2 ) );

		$red   = max( 0, min( 255, $red + $change ) );
		$green = max( 0, min( 255, $green + $change ) );
		$blue  = max( 0, min( 255, $blue + $change ) );

		return '#' . dechex( $red ) . dechex( $green ) . dechex( $blue );
	}
}
