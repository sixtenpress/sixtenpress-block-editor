<?php

/**
 * Class SixTenPressBlockEditorPostType
 */
class SixTenPressBlockEditorPostType {

	/**
	 * @var string $post_type
	 */
	protected $post_type = 'block_area';

	/**
	 * @var string $meta_key
	 */
	private $meta_key = 'sixtenpressblockeditor';

	/**
	 * Register the custom post type
	 * @since 0.4.0
	 */
	public function register() {
		$setting = sixtenpressblockeditor_get_setting( 'enable_block_areas' );
		if ( ! $setting ) {
			return;
		}
		$args = $this->get_post_type_args();
		if ( function_exists( 'sixtenpress_register_post_type' ) ) {
			$args['labels'] = array(
				'singular' => __( 'Block Area', 'sixtenpress-block-editor' ),
				'plural'   => __( 'Block Areas', 'sixtenpress-block-editor' ),
			);
			sixtenpress_register_post_type( $this->post_type, $args );

			return;
		}

		$args['labels'] = $this->get_post_type_labels();

		register_post_type( $this->post_type, $args );
	}

	/**
	 * Get the standard args for the post type.
	 * @return array
	 */
	private function get_post_type_args() {
		return array(
			'hierarchical'        => false,
			'supports'            => array( 'title', 'editor', 'revisions' ),
			'public'              => false,
			'show_ui'             => true,
			'show_in_rest'        => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'menu_icon'           => 'dashicons-welcome-widgets-menus',
			'menu_position'       => 25,
			'publicly_queryable'  => false,
			'capability'          => 'page',
			'show_in_nav_menus'   => false,
			'glancer'             => true,
		);
	}

	/**
	 * Get all labels for the post type.
	 * @return array
	 */
	private function get_post_type_labels() {
		return array(
			'name'               => __( 'Block Areas', 'sixtenpress-block-editor' ),
			'singular_name'      => __( 'Block Area', 'sixtenpress-block-editor' ),
			'add_new_item'       => __( 'Add New Block Area', 'sixtenpress-block-editor' ),
			'edit_item'          => __( 'Edit Block Area', 'sixtenpress-block-editor' ),
			'new_item'           => __( 'New Block Area', 'sixtenpress-block-editor' ),
			'view_item'          => __( 'View Block Area', 'sixtenpress-block-editor' ),
			'search_items'       => __( 'Search Block Areas', 'sixtenpress-block-editor' ),
			'not_found'          => __( 'No Block Areas found', 'sixtenpress-block-editor' ),
			'not_found_in_trash' => __( 'No Block Areas found in Trash', 'sixtenpress-block-editor' ),
			'parent_item_colon'  => __( 'Parent Block Area:', 'sixtenpress-block-editor' ),
			'menu_name'          => __( 'Block Areas', 'sixtenpress-block-editor' ),
		);
	}
}
