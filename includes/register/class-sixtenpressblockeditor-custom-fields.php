<?php

/**
 * Class SixTenPressBlockEditorCustomFields
 */
class SixTenPressBlockEditorCustomFields {

	/**
	 * @var string $post_type
	 */
	protected $post_type = 'block_area';

	/**
	 * @var string $meta_key
	 */
	private $meta_key = 'sixtenpressblockeditor';

	/**
	 * If possible, register custom fields.
	 * @since 0.4.0
	 */
	public function custom_fields() {
		$metabox = array( $this, 'get_metabox' );
		if ( version_compare( SIXTENPRESS_VERSION, '2.5.3', '<' ) ) {
			$metabox = $this->get_metabox();
		}
		sixtenpress_register_custom_fields(
			$this->post_type,
			$this->meta_key,
			$metabox
		);
	}

	/**
	 * Define the metabox.
	 * @since 0.4.0
	 *
	 * @return array
	 */
	public function get_metabox() {
		return array(
			'name'     => 'sixtenpress_block_editor',
			'label'    => __( 'Custom Block Area Settings', 'sixtenpress-block-editor' ),
			'context'  => 'side',
			'priority' => 'high',
			'fields'   => $this->get_custom_fields(),
		);
	}

	/**
	 * Get the custom fields.
	 * @since 0.4.0
	 *
	 * @return array
	 */
	private function get_custom_fields() {
		return array(
			array(
				'setting' => 'wrap',
				'type'    => 'checkbox',
				'label'   => __( 'Add wrap to block area', 'sixtenpress-block-editor' ),
				'default' => 0,
			),
			array(
				'setting' => 'class',
				'type'    => 'text',
				'label'   => __( 'Custom Block Area Class', 'sixtenpress-block-editor' ),
				'default' => '',
			),
			array(
				'setting' => 'background_hex',
				'type'    => 'color',
				'label'   => __( 'Custom Background Color', 'sixtenpress-block-editor' ),
				'default' => '',
			),
			array(
				'setting' => 'color_hex',
				'type'    => 'color',
				'label'   => __( 'Custom Text Color', 'sixtenpress-block-editor' ),
				'default' => '',
			),
			array(
				'setting' => 'background',
				'default' => '',
			),
			array(
				'setting' => 'color',
				'default' => '',
			),
		);
	}

	/**
	 * When the block area post type is loaded, check if the hex color needs to be updated based on the slug.
	 *
	 * @param object $post
	 * @param string $key
	 * @since 0.9.0
	 */
	public function initialize_fields( $post, $key ) {
		if ( $this->meta_key !== $key ) {
			return;
		}
		$meta  = get_post_meta( $post->ID, "_{$this->meta_key}", true );
		$keys  = array(
			'background' => 'background_hex',
			'color'      => 'color_hex',
		);
		$slugs = $this->get_color_slugs();
		$new   = array();
		foreach ( $keys as $slug_key => $hex_key ) {
			if ( empty( $meta[ $slug_key ] ) ) {
				continue;
			}
			if ( ! array_key_exists( $meta[ $slug_key ], $slugs ) ) {
				continue;
			}
			if ( isset( $meta[ $hex_key ] ) && ( $meta[ $hex_key ] === $slugs[ $meta[ $slug_key ] ] ) ) {
				continue;
			}
			$new[ $hex_key ] = $slugs[ $meta[ $slug_key ] ];
		}
		if ( ! $new ) {
			return;
		}
		update_post_meta( $post->ID, "_{$this->meta_key}", wp_parse_args( $new, $meta ), $meta );
	}

	/**
	 * Add the color slugs, if they exist, when the meta is being saved.
	 *
	 * @param array $post_value
	 * @param int $post_id
	 * @param array $post_meta
	 * @return array
	 * @since 0.9.0
	 */
	public function maybe_insert_meta( $post_value, $post_id, $post_meta ) {
		$slugs = $this->get_color_slugs();
		$keys  = array(
			'background_hex' => 'background',
			'color_hex'      => 'color',
		);
		foreach ( $keys as $hex => $slug ) {
			$post_value[ $slug ] = '';
			if ( in_array( $post_value[ $hex ], $slugs, true ) ) {
				$post_value[ $slug ] = array_search( $post_value[ $hex ], $slugs, true );
			}
		}

		return $post_value;
	}

	/**
	 * Get the editor color slugs and hex codes.
	 *
	 * @return array
	 * @since 0.9.0
	 */
	private function get_color_slugs() {
		$theme_support = sixtenpressblockeditor_get_theme_support_array();
		if ( ! $theme_support ) {
			return array();
		}
		$slugs = wp_list_pluck( $theme_support, 'color', 'slug' );
		foreach ( $slugs as $slug => &$hex ) {
			if ( 4 === strlen( $hex ) ) {
				$slugs[ $slug ] = $hex[0] . $hex[1] . $hex[1] . $hex[2] . $hex[2] . $hex[3] . $hex[3];
			}
		}

		return $slugs;
	}
}
