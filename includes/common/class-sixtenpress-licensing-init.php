<?php
/**
 * @package SixTenpress
 * @copyright 2019 Robin Cornett
 */

if ( ! class_exists( 'SixTenPressLicensing' ) ) {
	include_once 'class-sixtenpress-licensing.php';
}

/**
 * Class to initialize the Six/Ten Press Licensing class more easily.
 *
 * Class SixTenPressLicensingInit
 */
class SixTenPressLicensingInit extends SixTenPressLicensing {

	/**
	 * SixTenPressLicensingInit constructor.
	 *
	 * @param $args
	 */
	public function __construct( $args ) {
		$args = wp_parse_args( $args, $this->defaults() );
		if ( ! $args['basename'] ) {
			return;
		}
		$this->version  = $args['version'];
		$this->name     = $args['name'];
		$this->slug     = $args['slug'];
		$this->page     = $args['page'];
		$this->key      = $args['key'];
		$this->basename = $args['basename'];
		$this->url      = $args['url'];
		$this->author   = $args['author'];
		$this->item_id  = $args['item_id'];

		add_action( 'admin_init', array( $this, 'set_up_licensing' ), 999 );
	}

	/**
	 * Set up the defaults for the licensing class.
	 *
	 * @return array
	 */
	protected function defaults() {
		return array(
			'version'  => false,
			'name'     => __( 'Six/Ten Press', 'sixtenpress' ),
			'slug'     => false,
			'page'     => $this->page,
			'key'      => 'sixtenpress',
			'basename' => false,
			'url'      => $this->url, // this is set in the primary licensing class, so can be ignored.
			'author'   => $this->author, // this is set in the primary licensing class, so can be ignored.
			'item_id'  => null,
		);
	}

	/**
	 * Function to set up licensing fields and call the updater.
	 */
	public function set_up_licensing() {
		if ( ! $this->is_sixten_active() ) {
			if ( 'sixtenpress' === $this->page ) {
				$this->page = $this->key;
			}
			$this->action = "{$this->page}_save-settings";
			$this->nonce  = "{$this->page}_nonce";
		}
		$this->register_settings();
		$this->updater();
		add_action( "load-settings_page_{$this->page}", array( $this, 'build_settings_page' ), 20 );
		if ( $this->is_sixten_active() ) {
			add_action( 'sixtenpress_weekly_events', array( $this, 'weekly_license_check' ) );
			add_filter( 'sixtenpress_licensing_error_message', array( $this, 'sixtenpress_error_message' ) );
		}
	}

	/**
	 * Build the licensing settings page.
	 */
	public function build_settings_page() {
		if ( ! $this->is_sixten_active() ) {
			$this->add_sections( $this->register_section() );
		}
		$this->add_fields( $this->register_fields(), $this->register_section() );
	}

	/**
	 * Register the licensing section.
	 * @return array
	 */
	protected function register_section() {
		return array(
			'licensing' => array(
				'id'    => 'licensing',
				'tab'   => 'licensing',
				'title' => __( 'License(s)', 'sixtenpress' ),
			),
		);
	}
}
