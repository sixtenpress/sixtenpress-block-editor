<?php

/**
 * Class SixTenPressFieldRadio
 * @copyright 2019 Robin Cornett
 */
class SixTenPressFieldRadio extends SixTenPressFieldBase {

	/**
	 * Build a radio input field.
	 */
	public function do_field() {
		echo '<fieldset class="' . esc_attr( $this->get_fieldset_class() ) . '">';
		foreach ( $this->field['options'] as $choice => $field_label ) {
			echo '<div class="sixtenpress__control">';
			printf(
				'<input %5$s id="%4$s-%1$s" value="%1$s"%2$s><label for="%4$s-%1$s">%3$s</label>',
				esc_attr( $choice ),
				checked( $choice, $this->value, false ),
				esc_html( $field_label ),
				esc_attr( $this->id ),
				// phpcs:ignore
				implode( ' ', $this->get_input_attributes() )
			);
			echo '</div>';
		}
		echo '</fieldset>';
	}

	/**
	 * Gets the input attributes.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		$attributes = array(
			'type'            => 'radio',
			'name'            => $this->name,
			'aria-labelledby' => $this->id,
		);

		return $this->parse_attributes( $attributes );
	}
}
