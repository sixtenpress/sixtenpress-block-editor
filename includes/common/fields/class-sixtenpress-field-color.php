<?php

/**
 * Class SixTenPressFieldColor
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldColor extends SixTenPressFieldBase {

	/**
	 * Build a color field.
	 */
	public function do_field() {
		$this->load_scripts_styles();
		printf(
			'<input %s>',
			// phpcs:ignore
			implode( ' ', $this->get_input_attributes() )
		);
	}

	/**
	 * Gets the input attributes.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		return $this->parse_attributes(
			array(
				'type'               => 'text',
				'name'               => $this->name,
				'id'                 => $this->id,
				'value'              => $this->value,
				'class'              => 'color-field',
				'data-default-color' => $this->get_default(),
			)
		);
	}

	/**
	 * Get the default color value if it exists.
	 * @return string
	 */
	protected function get_default() {
		return ! empty( $this->field['default'] ) ? $this->field['default'] : '';
	}

	/**
	 * Enqueue/print the color picker scripts/styles.
	 * In 2.6.0, relocated to here for just-in-time loading instead of checking with filters.
	 *
	 * @since 2.6.0
	 */
	public function load_scripts_styles() {
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		$code = '( function( $ ) { \'use strict\'; $( function() { $( \'.color-field\' ).wpColorPicker( ' . wp_json_encode( $this->get_color_picker_options() ) . ' ); }); })( jQuery );';
		wp_add_inline_script( 'wp-color-picker', $code );
	}

	/**
	 * Gets the color picker options.
	 *
	 * @return array
	 */
	private function get_color_picker_options() {
		$options = array(
			'palettes' => $this->get_color_palettes(),
		);
		if ( ! empty( $this->field['args'] ) ) {
			$options = wp_parse_args( $this->field['args'], $options );
		}

		return $options;
	}

	/**
	 * Allow themes/devs to set custom palettes. Use the filter itself, or
	 * extract colors from explicitly declared Gutenberg color palette.
	 *
	 * @since 2.1.1
	 * @return mixed
	 */
	private function get_color_palettes() {
		list( $theme, ) = get_theme_support( 'editor-color-palette' );
		$palettes       = true;
		if ( is_array( $theme ) ) {
			$palettes = wp_list_pluck( $theme, 'color' );
			if ( empty( $palettes ) ) {
				$palettes = true;
			}
		}

		return apply_filters( 'sixtenpress_colorpicker_palettes', $palettes );
	}
}
