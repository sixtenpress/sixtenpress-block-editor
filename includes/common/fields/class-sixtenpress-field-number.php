<?php

/**
 * Build out the number field.
 *
 * Class SixTenPressFieldNumber
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldNumber extends SixTenPressFieldBase {

	/**
	 * Build a number field.
	 */
	public function do_field() {
		$screen      = get_current_screen();
		$this->field = $this->update_field( $screen );
		$input       = sprintf(
			'<input %s>',
			implode( ' ', $this->get_input_attributes() )
		);

		if ( 'post' !== $screen->base ) {
			$input = sprintf(
				'<label for="%s">%s%s</label>',
				esc_attr( $this->name ),
				$input,
				esc_attr( $this->field['label'] )
			);
		}

		// phpcs:ignore
		echo $input;
	}

	/**
	 * Update the field with defaults.
	 *
	 * @param object $screen
	 * @return array
	 */
	private function update_field( $screen ) {
		return wp_parse_args(
			$this->field,
			array(
				'class' => 'post' !== $screen->base ? 'small-text' : 'regular-text',
				'min'   => '',
				'max'   => '',
				'step'  => 1,
				'value' => '',
			)
		);
	}

	/**
	 * Build an array of attributes for the input field.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		$attributes = array(
			'type'       => 'text',
			'inputmode'  => 'decimal',
			'pattern'    => '[0-9]*',
			'id'         => $this->id,
			'aria-label' => $this->name,
			'name'       => $this->name,
			'value'      => $this->value,
			'class'      => $this->field['class'],
		);
		if ( ! empty( $this->field['min'] ) || ! empty( $this->field['max'] ) ) {
			$attributes['type'] = 'number';
			$attributes['step'] = $this->field['step'];
			$attributes['min']  = $this->field['min'];
			$attributes['max']  = $this->field['max'];
		}

		return $this->parse_attributes( $attributes );
	}
}
