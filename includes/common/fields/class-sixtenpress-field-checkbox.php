<?php

/**
 * Build out the checkbox field.
 *
 * Class SixTenPressFieldCheckbox
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldCheckbox extends SixTenPressFieldBase {

	/**
	 * Build a checkbox field.
	 */
	public function do_field() {
		printf(
			'<input type="hidden" name="%s" value="0">',
			esc_attr( $this->name )
		);

		printf(
			'<input %4$s%2$s><label for="%1$s">%3$s</label>',
			esc_attr( $this->id ),
			checked( 1, $this->value, false ),
			esc_attr( $this->field['label'] ),
			implode( ' ', $this->get_input_attributes() )
		);
	}

	/**
	 * Gets the input attributes.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		return $this->parse_attributes(
			array(
				'type'  => 'checkbox',
				'name'  => $this->name,
				'id'    => $this->id,
				'class' => 'code',
				'value' => 1,
			)
		);
	}
}
