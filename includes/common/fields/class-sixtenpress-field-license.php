<?php

/**
 * Build out the license key field.
 *
 * Class SixTenPressFieldLicense
 * @copyright 2018-2020 Robin Cornett
 * @since 2.5.0
 */
class SixTenPressFieldLicense extends SixTenPressFieldBase {

	/**
	 * The status option of the license ('sixtenpress_status').
	 *
	 * @var boolean
	 */
	private $status;

	/**
	 * Build a licensing key field.
	 *
	 * Note that the $name passed in the class construction is actually the
	 * key for the license options (eg 'sixtenpress' instead of 'sixtenpressfeaturedcontent').
	 */
	public function do_field() {

		echo '<div class="sixtenpress-license-field">';
		$this->activated_indicator();
		$this->do_input();
		echo '</div>';

		if ( ! empty( $this->value ) && 'valid' === $this->status() ) {
			$this->add_deactivation_button();
		}
		if ( 'valid' === $this->status() ) {
			return;
		}
		if ( ! $this->is_sixten_active() ) {
			$this->add_activation_button();
		}
		printf( '<p class="description"><label for="%1$s">%2$s</label></p>', esc_attr( $this->id ), esc_html( $this->field['label'] ) );
	}

	/**
	 * Maybe output the visual indicator for a license being active/valid.
	 *
	 * @since 2.5.0
	 */
	private function activated_indicator() {
		if ( 'valid' !== $this->status() ) {
			return;
		}
		$style = 'color:white;background-color:green;border-radius:100%;margin-right:8px;vertical-align:middle;';
		printf(
			'<span class="dashicons dashicons-yes" style="%s"><span class="screen-reader-text">%s</span></span>',
			esc_attr( $style ),
			esc_html__( 'Activated', 'sixtenpress' )
		);
	}

	/**
	 * Output the [password] field for the license key.
	 *
	 * @since 2.5.0
	 */
	private function do_input() {
		printf(
			'<input %s>',
			// phpcs:ignore
			implode( ' ', $this->get_input_attributes() )
		);
	}

	/**
	 * Gets the license field attributes.
	 *
	 * @since 2.6.3
	 * @return array
	 */
	protected function get_input_attributes() {
		return $this->parse_attributes(
			array(
				'type'       => 'password',
				'id'         => $this->id,
				'aria-label' => $this->id,
				'name'       => $this->id,
				'value'      => $this->value,
				'class'      => 'regular-text',
			)
		);
	}

	/**
	 * Define/output license activation button.
	 */
	private function add_activation_button() {

		if ( 'valid' === $this->status() ) {
			return;
		}

		$this->print_button(
			'button-primary',
			"{$this->name}_activate",
			__( 'Activate', 'sixtenpress' )
		);
	}

	/**
	 * Define/output license deactivation button.
	 */
	private function add_deactivation_button() {

		if ( 'valid' !== $this->status() ) {
			return;
		}

		$this->print_button(
			'button-secondary',
			"{$this->name}_deactivate",
			__( 'Deactivate', 'sixtenpress' )
		);
	}

	/**
	 * Print a submit button.
	 *
	 * @param $class
	 * @param $name
	 * @param $value
	 */
	private function print_button( $class, $name, $value ) {
		printf(
			'<input type="submit" class="%s" name="%s" value="%s">',
			esc_attr( $class ),
			esc_attr( $name ),
			esc_attr( $value )
		);
	}

	/**
	 * Get the license status (key passed in the class constructor).
	 *
	 * @return string
	 */
	private function status() {
		if ( isset( $this->status ) ) {
			return $this->status;
		}
		$this->status = isset( $this->field['args']['status'] ) ? $this->field['args']['status'] : get_option( "{$this->name}_status", false );

		return $this->status;
	}

	/**
	 * Check to see if 6/10 Press is active or not.
	 *
	 * @since 2.5.0
	 * @return boolean
	 */
	private function is_sixten_active() {
		return (bool) function_exists( 'sixtenpress_get_setting' );
	}
}
