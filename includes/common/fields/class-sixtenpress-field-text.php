<?php

/**
 * Build out the text field.
 *
 * Class SixTenPressFieldText
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldText extends SixTenPressFieldBase {

	/**
	 * Build a text field.
	 */
	public function do_field() {
		$defaults    = array(
			'class'  => 'regular-text',
			'format' => '',
		);
		$this->field = wp_parse_args( $this->field, $defaults );
		if ( 'custom_date' === $this->field['class'] || 'date' === $this->field['format'] ) {
			$this->load_datepicker_script_style();
			$this->value          = $this->value ? gmdate( 'n/j/Y', $this->value ) : '';
			$this->field['class'] = 'custom_date';
		}

		printf(
			'<input %1$s%2$s>',
			// phpcs:ignore
			implode( ' ', $this->get_input_attributes() ),
			// phpcs:ignore
			$this->get_data_attribute()
		);
	}

	/**
	 * Gets the input attributes for the text field.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		return $this->parse_attributes(
			array(
				'type'       => $this->get_field_type(),
				'id'         => $this->id,
				'aria-label' => $this->name,
				'name'       => $this->name,
				'value'      => $this->value,
				'class'      => $this->field['class'],
			)
		);
	}

	/**
	 * Get the correct field type (HTML5).
	 *
	 * @return string
	 */
	private function get_field_type() {
		if ( empty( $this->field['format'] ) ) {
			return 'text';
		}
		switch ( $this->field['format'] ) {
			case 'email':
				return 'email';

			case 'url':
				return 'url';

			case 'password':
				return 'password';

			default:
				return 'text';
		}
	}

	/**
	 * Allow plugins to add custom data attributes to an input field.
	 *
	 * @since 2.0.0
	 * @return string
	 */
	protected function get_data_attribute() {
		if ( ! isset( $this->field['data'] ) || ! $this->field['data'] ) {
			return '';
		}
		if ( is_string( $this->field['data'] ) ) {
			return ' ' . $this->field['data'];
		}
		$output = '';
		foreach ( $this->field['data'] as $key => $value ) {
			$output .= sprintf( ' %s="%s"', esc_attr( $key ), esc_attr( $value ) );
		}

		return $output;
	}

	/**
	 * Enqueue the date picker
	 */
	private function load_datepicker_script_style() {
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style( 'sixtenpress-datepicker', plugins_url( '/css/sixtenpress-datepicker.css', dirname( __DIR__ ) ), array( 'sixtenpress-postmeta' ), $this->version(), 'screen' );
		$minify = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_enqueue_script( 'sixtenpress-datepicker', plugins_url( "/js/datepicker{$minify}.js", dirname( __DIR__ ) ), array( 'jquery-ui-datepicker' ), $this->version(), 'screen' );
	}
}
