<?php

/**
 * Build out a textarea field.
 *
 * Class SixTenPressFieldTextarea
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldTextarea extends SixTenPressFieldBase {

	/**
	 * Build a textarea field.
	 */
	public function do_field() {
		printf(
			'<textarea %1$s>%2$s</textarea>',
			// phpcs:ignore
			implode( ' ', $this->get_input_attributes() ),
			// phpcs:ignore
			sanitize_textarea_field( $this->value )
		);
	}

	/**
	 * Gets the textarea input attributes.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		return $this->parse_attributes(
			array(
				'id'         => $this->id,
				'name'       => $this->name,
				'aria-label' => $this->field['label'],
				'class'      => 'large-text',
				'rows'       => isset( $this->field['rows'] ) ? $this->field['rows'] : 3,
			)
		);
	}
}
