<?php

/**
 * This is the base class for building Six/Ten Press fields.
 *
 * Class SixTenPressFieldBase
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldBase {

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $id;

	/**
	 * @var string|bool|int|array|mixed
	 */
	protected $value;

	/**
	 * @var array
	 */
	protected $field;

	/**
	 * SixTenPressFieldBase constructor.
	 *
	 * @param $name
	 * @param $id
	 * @param $value
	 * @param $field
	 */
	public function __construct( $name, $id, $value, $field ) {
		$this->name  = $name;
		$this->id    = $id;
		$this->value = $value;
		$this->field = $field;
		wp_enqueue_style( 'sixtenpress-postmeta' );
	}

	/**
	 * Gets the input attributes.
	 * These are defaults.
	 *
	 * @since 2.6.3
	 * @return array
	 */
	protected function get_input_attributes() {
		return array(
			'id'    => $this->id,
			'name'  => $this->name,
			'value' => $this->value,
			'class' => $this->field['class'],
		);
	}

	/**
	 * Parses the input attributes and converts them to an array ready to be imploded.
	 *
	 * @since 2.6.3
	 * @param array $attributes
	 * @return array
	 */
	protected function parse_attributes( $attributes ) {
		$tags = array();
		foreach ( $attributes as $key => $value ) {
			$tags[] = sprintf( '%s="%s"', esc_attr( $key ), esc_attr( $value ) );
		}
		if ( ! empty( $this->field['required'] ) ) {
			$tags[] = 'required';
		}
		if ( ! empty( $this->field['readonly'] ) ) {
			$tags[] = 'readonly';
		}
		if ( ! empty( $this->field['disabled'] ) ) {
			$tags[] = 'disabled';
		}

		return $tags;
	}

	/**
	 * Get the plugin version if it's set.
	 *
	 * @return string
	 * @since 2.6.0
	 */
	protected function version() {
		return defined( 'SIXTENPRESS_VERSION' ) ? SIXTENPRESS_VERSION : '';
	}

	/**
	 * Get the fieldset class.
	 *
	 * @return string
	 */
	protected function get_fieldset_class() {
		$class = 'sixtenpress__fieldset';
		if ( empty( $this->field['clear'] ) ) {
			$class .= '--inline';
		}

		return $class;
	}
}
