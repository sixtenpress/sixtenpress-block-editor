<?php

/**
 * Class SixTenPressFieldEnhanced
 * @copyright 2020 Robin Cornett
 */
class SixTenPressFieldEnhanced extends SixTenPressFieldBase {

	/**
	 * Build a multiselect field.
	 */
	public function do_field() {
		$this->load_selectwoo();
		// phpcs:ignore
		printf( '<select %s>', implode( ' ', $this->get_input_attributes() ) );
		foreach ( (array) $this->field['options'] as $option => $field_label ) {
			if ( $this->is_multiple() && empty( $option ) ) {
				continue;
			}
			printf(
				'<option value="%s" %s>%s</option>',
				esc_attr( $option ),
				selected( $option, $this->update_value( $option ), false ),
				esc_attr( $field_label )
			);
		}
		echo '</select>';
	}

	/**
	 * Is this a multiselect field?
	 *
	 * @return boolean
	 */
	private function is_multiple() {
		return 'multiselect' === $this->field['type'];
	}

	/**
	 * Updates the input field value if multiselect.
	 *
	 * @param string|int $value
	 * @return string|int
	 */
	private function update_value( $value ) {
		if ( ! $this->is_multiple() ) {
			return $this->value;
		}

		return is_array( $this->value ) && in_array( $value, $this->value, true ) ? $value : '';
	}

	/**
	 * Gets the select input attributes.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		$attributes = array(
			'id'         => $this->id,
			'aria-label' => $this->get_aria(),
			'name'       => $this->name,
			'class'      => $this->get_class(),
		);
		if ( $this->is_multiple() ) {
			$attributes['name']    .= '[]';
			$attributes['multiple'] = 'multiple';
		}

		return $this->parse_attributes( $attributes );
	}

	/**
	 * Defines the classes for the selectWoo field.
	 *
	 * @return string
	 */
	private function get_class() {
		$classes = array(
			'sixtenpress-enhanced',
		);
		if ( $this->is_multiple() ) {
			$classes[] = 'widefat';
		}
		if ( ! empty( $this->field['class'] ) ) {
			if ( is_string( $this->field['class'] ) ) {
				$passed_classes = explode( ',', $this->field['class'] );
			}
			$classes = array_merge( $passed_classes, $classes );
		}

		return implode( ' ', $classes );
	}

	/**
	 * Define the aria-label value for the select field.
	 *
	 * @return string
	 */
	private function get_aria() {
		$aria = $this->name;
		if ( isset( $this->field['label'] ) ) {
			$aria = $this->field['label'];
		} elseif ( isset( $this->field['title'] ) ) {
			$aria = $this->field['title'];
		}

		return $aria;
	}

	/**
	 * If a select field is enhanced, enqueue Select2 script/style.
	 *
	 * @since 2.6.0
	 */
	private function load_selectwoo() {
		$select2_version = '1.0.5';
		wp_enqueue_style( 'selectwoo', plugins_url( '/css/select2.css', dirname( __DIR__ ) ), array( 'sixtenpress-postmeta' ), $select2_version, 'screen' );
		wp_enqueue_script( 'selectwoo', plugins_url( '/js/selectWoo.min.js', dirname( __DIR__ ) ), array( 'jquery' ), $select2_version, 'screen' );
		wp_add_inline_script( 'selectwoo', 'jQuery(document).ready(function($) {$(\'select.sixtenpress-enhanced\').selectWoo();});' );
	}
}
