<?php

/**
 * Build out the select field.
 *
 * Class SixTenPressFieldSelect
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldSelect extends SixTenPressFieldBase {

	/**
	 * Build a select field.
	 */
	public function do_field() {
		// phpcs:ignore
		printf( '<select %s>', implode( ' ', $this->get_input_attributes() ) );
		foreach ( (array) $this->field['options'] as $option => $field_label ) {
			printf(
				'<option value="%s" %s>%s</option>',
				esc_attr( $option ),
				selected( $option, $this->value, false ),
				esc_attr( $field_label )
			);
		}
		echo '</select>';
	}

	/**
	 * Gets the select input attributes.
	 *
	 * @return array
	 */
	protected function get_input_attributes() {
		$attributes = array(
			'id'         => $this->id,
			'aria-label' => $this->get_aria(),
			'name'       => $this->name,
		);
		$class      = $this->get_class();
		if ( $class ) {
			$attributes['class'] = $class;
		}

		return $this->parse_attributes( $attributes );
	}

	/**
	 * Define the optional class for the select field.
	 *
	 * @return string
	 */
	private function get_class() {
		$classes = array();
		if ( ! empty( $this->field['class'] ) ) {
			if ( is_string( $this->field['class'] ) ) {
				$classes = explode( ',', $this->field['class'] );
			}
		}

		return implode( ' ', $classes );
	}

	/**
	 * Define the aria-label value for the select field.
	 *
	 * @return string
	 */
	private function get_aria() {
		$aria = $this->name;
		if ( isset( $this->field['label'] ) ) {
			$aria = $this->field['label'];
		} elseif ( isset( $this->field['title'] ) ) {
			$aria = $this->field['title'];
		}

		return $aria;
	}
}
