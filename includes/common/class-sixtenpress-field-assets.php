<?php

class SixTenPressFieldAssets {

	/**
	 * Enqueue the custom style for the post meta fields.
	 * @since 2.6.0
	 */
	public function load_styles() {
		wp_register_style( 'sixtenpress-postmeta', esc_url( plugins_url( '/includes/css/sixtenpress-postmeta.css', dirname( __DIR__ ) ) ), array(), $this->version(), 'screen' );
		$this->uploader();
	}

	/**
	 * Enqueue the file uploader script.
	 * @since 2.6.0
	 */
	private function uploader() {
		wp_enqueue_media();
		wp_register_script(
			'sixtenpress-upload',
			plugins_url( "/includes/js/file-upload{$this->minify()}.js", dirname( __DIR__ ) ),
			array( 'jquery', 'media-upload', 'thickbox' ),
			$this->version(),
			true
		);
		wp_register_style( 'sixtenpress-upload', plugins_url( '/includes/css/sixtenpress-upload.css', dirname( __DIR__ ) ), array( 'sixtenpress-postmeta' ), $this->version(), 'screen' );
		add_action( 'admin_footer', array( $this, 'localize_uploader' ) );
	}

	/**
	 * Send off the localized script data.
	 */
	public function localize_uploader() {
		$data = apply_filters( 'sixtenpress_uploader_localization', array() );
		if ( empty( $data ) ) {
			return;
		}
		$args = array(
			'class' => 'file',
		);
		wp_localize_script( 'sixtenpress-upload', 'SixTenUpload', array_merge( $data, $args ) );
	}

	/**
	 * Enqueue the post meta script.
	 */
	public function repeaters() {
		wp_enqueue_script(
			'sixtenpress-repeaters',
			plugins_url( "/includes/js/post-meta{$this->minify()}.js", dirname( __DIR__ ) ),
			array(
				'jquery',
				'jquery-ui-sortable',
			),
			$this->version(),
			true
		);
		wp_localize_script( 'sixtenpress-repeaters', 'SixTenRepeaters', $this->get_post_meta_localization() );
	}

	/**
	 * Get the localization args for the post meta script.
	 *
	 * @return array
	 */
	private function get_post_meta_localization() {
		$labels = $this->get_post_meta_labels();
		return array(
			'confirm'      => $labels['confirm'],
			'color_picker' => false,
			'buttons'      => array(
				'add'    => array(
					'icon'  => 'dashicons-plus-alt',
					'class' => 'add-group',
					'label' => $labels['add'],
				),
				'remove' => array(
					'icon'  => 'dashicons-dismiss',
					'class' => 'remove-group',
					'label' => $labels['remove'],
				),
				'up'     => array(
					'icon'  => 'dashicons-arrow-up-alt2',
					'class' => 'shift shift-up',
					'label' => $labels['up'],
				),
				'down'   => array(
					'icon'  => 'dashicons-arrow-down-alt2',
					'class' => 'shift shift-down',
					'label' => $labels['down'],
				),
				'clear'   => array(
					'icon'  => 'dashicons-dismiss',
					'class' => 'clear-group',
					'label' => $labels['remove'],
				),
			),
			'toggle'       => array(
				'icon'  => 'dashicons-admin-generic',
				'class' => 'toggle',
				'label' => $labels['toggle'],
			),
			'move'         => array(
				'icon'  => 'dashicons-move',
				'class' => 'move-group',
				'label' => $labels['move'],
			),
		);
	}

	/**
	 * Set the labels for buttons, confirmations, etc. for post meta.
	 * @return array
	 */
	private function get_post_meta_labels() {
		return apply_filters(
			'sixtenpress_post_meta_labels',
			array(
				'confirm' => __( 'Are you sure you want to delete this row?', 'sixtenpress' ),
				'add'     => __( 'Add Row', 'sixtenpress' ),
				'remove'  => __( 'Remove Row', 'sixtenpress' ),
				'up'      => __( 'Shift Row Up', 'sixtenpress' ),
				'down'    => __( 'Shift Row Down', 'sixtenpress' ),
				'toggle'  => __( 'Section Settings', 'sixtenpress' ),
				'move'    => __( 'Move Row', 'sixtenpress' ),
			)
		);
	}

	/**
	 * Get minified file names.
	 *
	 * @return string
	 */
	protected function minify() {
		return defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	}

	/**
	 * Get the plugin version.
	 *
	 * @return string
	 * @since 2.6.0
	 */
	private function version() {
		return defined( 'SIXTENPRESS_VERSION' ) ? SIXTENPRESS_VERSION : '';
	}
}
