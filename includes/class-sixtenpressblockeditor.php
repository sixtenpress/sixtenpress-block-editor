<?php

/**
 * Class SixTenPressBlockEditor
 */
class SixTenPressBlockEditor {

	/**
	 * @var $enqueue \SixTenPressBlockEditorOutputEnqueue
	 */
	protected $enqueue;

	/**
	 * @var $register \SixTenPressBlockEditorPostType
	 */
	protected $register;

	/**
	 * The custom fields class.
	 *
	 * @var $fields \SixTenPressBlockEditorCustomFields
	 */
	protected $fields;

	/**
	 * @var $theme_support \SixTenPressBlockEditorThemeSupport
	 */
	protected $theme_support;

	/**
	 * @var $filters \SixTenPressBlockEditorOutputFilters
	 */
	protected $filters;

	/**
	 * SixTenPressBlockEditor constructor.
	 *
	 * @param $output
	 * @param $register
	 * @param $theme_support
	 */
	public function __construct( $output, $register, $fields, $theme_support, $filters ) {
		$this->enqueue       = $output;
		$this->register      = $register;
		$this->fields        = $fields;
		$this->theme_support = $theme_support;
		$this->filters       = $filters;
	}

	/**
	 * Initialize the plugin.
	 */
	public function init() {
		add_action( 'plugins_loaded', array( $this, 'load_settings' ) );
		add_action( 'admin_init', array( $this, 'licensing' ), 5 );
		add_action( 'customize_register', array( $this, 'customizer' ) );
		add_action( 'admin_menu', array( $this, 'add_reusable_blocks_menu' ) );
		add_action( 'admin_menu', array( $this, 'onboarding' ) );

		add_action( 'enqueue_block_editor_assets', array( $this->enqueue, 'block_editor_styles' ), 100 );
		add_action( 'enqueue_block_editor_assets', array( $this->enqueue, 'block_editor_scripts' ) );
		add_action( 'enqueue_block_editor_assets', array( $this->enqueue, 'block_editor_attributes' ) );
		add_action( 'enqueue_block_assets', array( $this->enqueue, 'block_library' ), 20 );
		add_action( 'enqueue_block_assets', array( $this->enqueue, 'inline_style' ), 25 );
		add_action( 'init', array( $this->enqueue, 'maybe_unregister_blocks' ), 1000 );

		add_action( 'init', array( $this->theme_support, 'modify_theme_support' ), 0 );

		add_action( 'init', array( $this->register, 'register' ) );
		add_action( 'sixtenpress_fields_do_metabox', array( $this->fields, 'initialize_fields' ), 10, 2 );
		add_filter( 'sixtenpress_sanitize__sixtenpressblockeditor_array', array( $this->fields, 'maybe_insert_meta' ), 10, 3 );
		add_action( 'sixtenpress_load_fields', array( $this->fields, 'custom_fields' ) );

		add_filter( 'render_block', array( $this->filters, 'gallery' ), 10, 2 );
	}

	/**
	 * Check for settings class and create the settings page.
	 */
	public function load_settings() {
		if ( ! class_exists( 'SixTenPressSettings' ) ) {
			include_once 'common/class-sixtenpress-settings.php';
		}
		add_filter( 'plugin_action_links_' . SIXTENPRESSBLOCKEDITOR_BASENAME, array( $this, 'add_settings_link' ) );
		include_once 'settings/class-sixtenpressblockeditor-settings.php';

		$settings = new SixTenPressBlockEditorSettings();
		add_filter( 'sixtenpressblockeditor_get_setting', array( $settings, 'get_setting' ) );
		add_action( 'admin_menu', array( $settings, 'maybe_add_settings_page' ), 20 );
		add_action( 'sixtenpress_settings_before_colors_input', array( $settings, 'modify_group_field_label' ), 10, 3 );
		add_action( 'sixtenpress_settings_before_fonts_input', array( $settings, 'modify_fonts_field_label' ), 10, 3 );
		add_filter( 'sixtenpress_sanitize_sixtenpressblockeditor_fonts', array( $settings, 'cast_font_to_integer' ) );
	}

	/**
	 * Add certain block editor settings to the customizer.
	 */
	public function customizer() {
		include_once 'settings/class-sixtenpressblockeditor-settings-customize.php';
		$customize = new SixTenPressSettingsBlockEditorCustomize();
		add_filter( 'sixtenpress_customizer_sections', array( $customize, 'add_sections' ) );
		add_filter( 'sixtenpress_customizer_fields', array( $customize, 'add_fields' ) );
	}

	/**
	 * Instantiate licensing class/updater
	 */
	public function licensing() {
		if ( ! class_exists( 'SixTenPressLicensingInit' ) ) {
			include_once 'common/class-sixtenpress-licensing-init.php';
		}

		new SixTenPressLicensingInit( $this->get_licensing_args() );
	}

	/**
	 * Build the array of args for the licensing class.
	 *
	 * @return array
	 * @since 0.7.0
	 */
	private function get_licensing_args() {
		return array(
			'version'  => SIXTENPRESSBLOCKEDITOR_VERSION,
			'name'     => __( 'Six/Ten Press Block Editor', 'sixtenpress-block-editor' ),
			'slug'     => 'sixtenpress-block-editor',
			'key'      => 'sixtenpressblockeditor',
			'basename' => SIXTENPRESSBLOCKEDITOR_BASENAME,
			'url'      => 'https://robincornett.com/edd-sl-api/',
			'item_id'  => 12357,
		);
	}

	/**
	 * If an onboarding page has been created, start the process.
	 *
	 * @since 2.7.0
	 */
	public function onboarding() {
		$page = sixtenpressblockeditor_get_setting( 'onboarding_post' );
		if ( ! $page ) {
			return;
		}
		include_once 'output/class-sixtenpressblockeditor-onboarding.php';
		$onboarding = new SixTenPressBlockEditorOnboarding( $page );
		$onboarding->add_submenu_page();
		add_action( 'admin_head-tools_page_sixtenpress-info', array( $this->enqueue, 'block_library' ), 20 );
	}

	/**
	 * Reusable Blocks accessible in backend
	 * @link https://www.billerickson.net/reusable-blocks-accessible-in-wordpress-admin-area
	 *
	 */
	public function add_reusable_blocks_menu() {
		add_menu_page(
			__( 'Reusable Blocks', 'sixtenpress-block-editor' ),
			__( 'Reusable Blocks', 'sixtenpress-block-editor' ),
			'manage_categories',
			'edit.php?post_type=wp_block',
			'',
			'dashicons-editor-table',
			30
		);
	}

	/**
	 * Add link to plugin settings page in plugin table
	 *
	 * @param $links array
	 *
	 * @return array
	 *
	 * @since 0.5.0
	 */
	public function add_settings_link( $links ) {
		$query_args = add_query_arg(
			$this->get_query_args(),
			admin_url( 'options-general.php' )
		);
		$links[]    = sprintf(
			'<a href="%s">%s</a>',
			esc_url( $query_args ),
			esc_attr__( 'Settings', 'sixtenpress-block-editor' )
		);

		return $links;
	}

	/**
	 * Get the correct query args for the settings page.
	 * @return array
	 */
	private function get_query_args() {
		$args = array(
			'page' => 'sixtenpressblockeditor',
		);
		if ( ! function_exists( 'sixtenpress_get_setting' ) ) {
			return $args;
		}

		return array(
			'page' => 'sixtenpress',
			'tab'  => 'blockeditor',
		);
	}
}
