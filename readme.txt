=== Six/Ten Press Block Editor ===

Contributors: littler.chicken
Donate link: https://robincornett.com/donate/
Tags: gutenberg, block editor
Requires at least: 5.2
Requires PHP: 5.6.20
Tested up to: 5.7
Stable tag: 1.1.2
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt

A small plugin to modify block editor styles and behavior.

== Description ==

_Six/Ten Press Block Editor_ provides less opinionated block editor styles, plus block areas which can be used sitewide, and acts as a "shim" for older themes to make better use of the block editor.

If your theme does not support custom editor colors and/or font sizes, you can define them on the plugin settings page to make your theme/brand colors easily available for any block (this is especially helpful for themes built before the block editor was released).

The plugin also adds some custom attributes to certain blocks to allow for easier styling when the plugin styles are active:
* custom height classes for cover blocks, columns, and groups (under the Advanced panel for each block)
* toggle to prevent columns from stacking on mobile
* custom margin for galleries, to change spacing between images

== Installation ==

1. Upload the entire `sixtenpress-block-editor` folder to your `/wp-content/plugins` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.
1. Visit the Settings > Six/Ten Press Block Editor page to change the default behavior of the plugin.

== Frequently Asked Questions ==

= How do I add a Block Area to my site with code? =

All that is really needed is a call to the `sixtenpressblockeditor_area()->show()` function, with a required parameter of either the block area ID or slug. With an ID:

	sixtenpressblockeditor_area()->show( 14 );

Here's an example of how you could set up default block areas in your code:

	add_action( 'init', 'leaven_maybe_run_block_editor_areas' );
	/**
	 * Check if the block area plugin is active.
	 * If so, remove the Primary Sidebar, then the Genesis code to output it,
	 * and replace it with the 'primary' block area.
	 */
	function leaven_maybe_run_block_editor_areas() {
		if ( ! function_exists( 'sixtenpressblockeditor_area' ) ) {
			return;
		}
		// Unregister the primary sidebar
		unregister_sidebar( 'sidebar' );

		// Remove the default primary sidebar output
		remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );

		add_action( 'genesis_sidebar', 'leaven_primary_block_area' );
	}

	/**
	 * Replace the primary sidebar output with the primary block area if it exists.
	 */
	function leaven_primary_block_area() {
		if ( leaven_check_block_editor_setting( 'genesis_sidebar' ) ) {
			return;
		}
		sixtenpressblockeditor_area()->show( 'primary' );
	}

	/**
	 * Helper function to get the block editor plugin setting and see if a
	 * block area has already been created/assigned to this hook.
	 * This allows the block areas which have been added via code to be
	 * overridden or replaced with block areas added on the plugin settings page.
	 *
	 * @param $hook
	 * @return bool
	 */
	function leaven_check_block_editor_setting( $hook ) {
		$setting = sixtenpressblockeditor_get_setting( 'block_areas' );

		return in_array( $hook, array_column( $setting, 'hook' ), true );
	}

== Screenshots ==

== Upgrade Notice ==

1.1.2: improved 5.7 compatibility

== Changelog ==

= 1.1.2 =
* updated: tested to WordPress 5.7
* updated: cover, buttons block styles for WP 5.7 compatibility
* updated: gallery style uses gap instead of margin

= 1.1.1 =
* updated: minimum WordPress version is 5.2
* fixed: columns' responsive control moved to advanced controls
* fixed: block area menu page permissions
* fixed: post type specific block area behavior on search results
* changed: color samples on settings page now use inline styles

= 1.1.0 =
* added: setting to disable custom gradients (in WordPress 5.4)
* added: advanced setting for Cover and Group blocks to limit inner-container width
* updated: CSS for default gradients, CSS for cover, media/text, columns, latest posts, gallery, search, paragraph, social links to be in line with WP 5.4 styles
* changed: moved gallery margin setting to block "advanced" fields

= 1.0.2 =
* added: admin menu link for reusable blocks
* updated: CSS for cover, media/text, gallery, separator, button blocks
* updated: prevent block areas from outputting on landing pages

= 1.0.1 =
* fixed: editor font size support for unusual sizes for themes which don't register their own sizes

= 1.0.0 =
* added: custom colors/font sizes can be used without the plugin style
* changed: default WP colors added to site only if custom colors are not registered
* updated: significant CSS changes to media/text block, simplified styles on columns, and updated CSS for button blocks
* fixed: editor font sizes misbehaving in the editor
* fixed: custom attribute script in editor
* fixed: block area query reset
* removed: deprecated shortName for font sizes

= 0.9.0 =
* improved: block area colors now use editor defined or custom colors
* updated: styles for galleries, cover block, image, search, button, media/text for better display, plus IE
* changed: columns block responsive toggle (now under Advanced, instead of Styles)
* changed: default font sizes are only added to CSS if no sizes are explicitly defined
* changed: custom heights are now available for cover blocks
* fixed: full/wide alignment styles for themes which don't naturally support it
* fixed: how theme support for colors/fonts are added, so that WordPress defaults may show

= 0.8.1 =
* fixed: cover image height with unexpectedly proportioned images
* removed: text-size CSS classes added in error

= 0.8.0 =
* added: filters for developers to easily add or remove block styles using a PHP filter
* added: filter to allow developers to remove blocks registered with JavaScript
* added: CSS for new image circle style (per Gutenberg 6.4.0)
* added: CSS for new social links block, changed galleries (Gutenberg 6.5.0)
* added: custom heights/padding for group/column blocks when using plugin stylesheet
* added: method for getting a block area without echoing it
* updated: CSS for separator, cover, and table blocks (per Gutenberg 6.4.0)
* updated: block area query
* fixed: possible issue with not being able to activate featured content addons if 6/10 is not active

= 0.7.1 =
* fixed: license key not being passed to server for updates

= 0.7.0 =
* added: customizer control to enable/disable plugin stylesheet (requires 6/10 Press 2.5.0)
* added: glancer icon for block areas (requires 6/10 Press 2.5.0)
* updated: custom styles for new text alignment classes, alignment, other block adjustments
* improved: licensing checks/updates
* fixed: custom color/font size theme support

= 0.6.0 =
* added: custom style options for columns block
* improved: general CSS for columns, button blocks
* fixed: block areas no longer appear in nav menus

= 0.5.0 =
* added: block area display settings include content type
* enabled: licensing/updating
* improved: block area retrieval and display behavior
* updated: cover image block CSS

= 0.4.1 =
* fixed: custom editor color/font size will not output if missing the slug
* fixed: block areas are now optional, enabled by a setting
* updated: CSS for video block, latest posts, cover, and group blocks

= 0.4.0 =
* added: block areas custom content type for replacing widget areas (beta)
* added: settings to enable wide/full align blocks; responsive embeds for themes which don't already support
* added: Genesis layout class helper in the editor (props Nick Cernis)
* updated: styles for search blocks, initial group block
* fixed: last color/font being removed on initial save

= 0.3.2 =
* updated: cover image block styles for new inner blocks
* updated: CSS for new column settings support
* fixed: 6/10 Press version check

= 0.3.1 =
* updated: block styles
* updated: settings descriptions

= 0.3.0 =
* added: setting to make overriding the Core block editor style optional
* updated: use custom colors/fonts without 6/10 Press
* updated: theme colors are no longer saved as part of the plugin settings

= 0.2.0 =
* added: custom settings page with settings to disable custom colors and font sizes
* added: custom editor color palette builder (requires 6/10 Press 2.3.0 )
* added/improved block editor styles

= 0.1.0 =
* Initial release
