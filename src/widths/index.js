/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
const { addFilter } = wp.hooks;
const { Fragment } = wp.element;
const { InspectorAdvancedControls } = wp.blockEditor;
const { createHigherOrderComponent } = wp.compose;
const { SelectControl } = wp.components;

//restrict to specific block names
const allowedBlocks = SixTenPressBlockEditorAttributes.widthBlocks;

/**
 * Add custom attribute for specific widths.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addAttributes( settings ) {

	//check if object exists for old Gutenberg version compatibility
	//add allowedBlocks restriction
	if ( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ) {
		settings.attributes = Object.assign( settings.attributes, {
			hasCustomwidth: {
				type: 'string',
				default: '',
			}
		} );
	}

	return settings;
}

/**
 * Add width controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {

		const {
			name,
			attributes,
			isSelected,
			clientId
		} = props;

		const {
			hasCustomwidth,
		} = attributes;

		if ( allowedBlocks.includes( name ) ) {
			jQuery( [ 'cover', 'group' ] ).each( function ( index, block ) {
				const $block = jQuery( '.wp-block-' + block + '#block-' + clientId );
				$block.removeClass( function ( index, className ) {
					return ( className.match( /(^|\s)has-width-\S+/g ) || [] ).join( ' ' );
				} )
				if ( hasCustomwidth ) {
					$block.addClass( 'has-width-' + hasCustomwidth );
				}
			} );
		}

		return (
			<Fragment>
				<BlockEdit {...props} />
				{isSelected && allowedBlocks.includes( name ) &&
					<InspectorAdvancedControls>
						<SelectControl
							options={SixTenPressBlockEditorAttributes.widthOptions}
							onChange={( value ) => {
								props.setAttributes( {
									hasCustomwidth: value,
								} );
							}}
							label={SixTenPressBlockEditorAttributes.widthLabel}
							value={hasCustomwidth}
						/>
					</InspectorAdvancedControls>
				}

			</Fragment>
		);
	};
}, 'withAdvancedControls' );

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass( extraProps, blockType, attributes ) {

	if ( !allowedBlocks.includes( blockType.name ) ) {
		return extraProps;
	}

	const { hasCustomwidth } = attributes;

	if ( typeof hasCustomwidth !== 'undefined' && hasCustomwidth ) {
		extraProps.className = classnames( extraProps.className, 'has-width-' + hasCustomwidth );
	}

	return extraProps;
}

addFilter(
	'blocks.registerBlockType',
	'sixtenpress/custom-attributes',
	addAttributes
);

addFilter(
	'editor.BlockEdit',
	'sixtenpress/custom-advanced-control',
	withAdvancedControls
);

addFilter(
	'blocks.getSaveContent.extraProps',
	'sixtenpress/applyExtraClass',
	applyExtraClass
);
