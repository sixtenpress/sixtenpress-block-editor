
/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress Dependencies
 */
const { addFilter } = wp.hooks;
const { Fragment } = wp.element;
const { InspectorAdvancedControls } = wp.blockEditor;
const { createHigherOrderComponent } = wp.compose;
const { ToggleControl } = wp.components;

//restrict to specific block names
const allowedBlocks = [ 'core/columns' ];

/**
 * Add custom attribute for specific heights.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addColumnAttributes( settings ) {

	//check if object exists for old Gutenberg version compatibility
	if ( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ) {
		settings.attributes = Object.assign( settings.attributes, {
			doesNotStack: {
				type: 'boolean',
				default: false,
			}
		} );
	}

	return settings;
}

/**
 * Add height controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const mobileAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {

		const {
			name,
			attributes,
			isSelected,
		} = props;

		const {
			doesNotStack,
		} = attributes;

		return (
			<Fragment>
				<BlockEdit {...props} />
				{isSelected && allowedBlocks.includes( name ) &&
					<InspectorAdvancedControls>
						<ToggleControl
							label={SixTenPressBlockEditorAttributes.responsive.label}
							checked={!!doesNotStack}
							onChange={() => props.setAttributes( { doesNotStack: !doesNotStack } )}
							help={!!doesNotStack ? SixTenPressBlockEditorAttributes.responsive.on : SixTenPressBlockEditorAttributes.responsive.off}
						/>
					</InspectorAdvancedControls>
				}

			</Fragment>
		);
	};
}, 'mobileAdvancedControls' );

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyMobileClass( extraProps, blockType, attributes ) {

	if ( !allowedBlocks.includes( blockType.name ) ) {
		return extraProps;
	}

	const { doesNotStack } = attributes;

	//check if attribute exists for old Gutenberg version compatibility
	//add class only when hasCustomHeight = false
	if ( typeof doesNotStack !== 'undefined' && doesNotStack ) {
		extraProps.className = classnames( extraProps.className, 'is-style-not-stacked' );
	}

	return extraProps;
}

addFilter(
	'blocks.registerBlockType',
	'sixtenpress/custom-column-attributes',
	addColumnAttributes
);

addFilter(
	'editor.BlockEdit',
	'sixtenpress/custom-advanced-control',
	mobileAdvancedControls
);

addFilter(
	'blocks.getSaveContent.extraProps',
	'sixtenpress/applyMobileClass',
	applyMobileClass
);
