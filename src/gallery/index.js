
/**
 * WordPress Dependencies
 */
const { addFilter } = wp.hooks;
const { Fragment } = wp.element;
const { InspectorAdvancedControls } = wp.blockEditor;
const { createHigherOrderComponent, withState } = wp.compose;
const { RangeControl } = wp.components;

//restrict to specific block names
const allowedBlocks = [ 'core/gallery' ];

/**
 * Add custom attribute for specific heights.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addGalleryAttributes( settings ) {

	//check if object exists for old Gutenberg version compatibility
	if ( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ) {
		settings.attributes = Object.assign( settings.attributes, {
			galleryMargin: {
				type: 'number',
				default: 4,
			}
		} );
	}

	return settings;
}

/**
 * Add height controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const galleryAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {

		const {
			name,
			attributes,
			isSelected,
		} = props;

		const {
			galleryMargin,
		} = attributes;
		if ( galleryMargin ) {
			jQuery( '.wp-block-gallery' ).attr( 'style', '--block-gallery-grid-size: ' + galleryMargin + 'px;' )
		}

		return (
			<Fragment>
				<BlockEdit {...props} />
				{isSelected && allowedBlocks.includes( name ) &&
					<InspectorAdvancedControls>
						<RangeControl
							label={SixTenPressBlockEditorAttributes.gallery.label}
							value={galleryMargin}
							onChange={value => ( props.setAttributes( { galleryMargin: value } ) )}
							min={0}
							max={24}
						/>
					</InspectorAdvancedControls>
				}

			</Fragment>
		);
	};
}, 'galleryAdvancedControls' );

addFilter(
	'blocks.registerBlockType',
	'sixtenpress/custom-gallery-margin',
	addGalleryAttributes
);

addFilter(
	'editor.BlockEdit',
	'sixtenpress/custom-gallery-margin',
	galleryAdvancedControls
);
