<?php

/**
 * Six/Ten Press Block Editor
 * @package SixTenPressBlockEditor
 * @author  Robin Cornett
 * @license GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Six/Ten Press Block Editor
 * Plugin URI:  https://robincornett.com/downloads/sixtenpress-block-editor
 * Description: A small plugin to modify block editor styles and behavior.
 * Version:     1.1.2
 * Author:      Robin Cornett
 * Author URI:  https://robincornett.com/
 * Text Domain: sixtenpress-block-editor
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Requires at least: 5.0
 * Domain Path: /languages
 */

// if this file is called directly abort
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'SIXTENPRESSBLOCKEDITOR_BASENAME' ) ) {
	/**
	 * Constant for plugin basename.
	 */
	define( 'SIXTENPRESSBLOCKEDITOR_BASENAME', plugin_basename( __FILE__ ) );
}

if ( ! defined( 'SIXTENPRESSBLOCKEDITOR_VERSION' ) ) {
	/**
	 * Constant for the plugin version.
	 */
	define( 'SIXTENPRESSBLOCKEDITOR_VERSION', '1.1.2' );
}

function sixtenpressblockeditor_require() {
	$files = array(
		'class-sixtenpressblockeditor',
		'class-sixtenpressblockeditor-theme-support',
		'output/class-sixtenpressblockeditor-output-block-area',
		'output/class-sixtenpressblockeditor-output-enqueue',
		'output/class-sixtenpressblockeditor-output-filters',
		'register/class-sixtenpressblockeditor-post-type',
		'register/class-sixtenpressblockeditor-custom-fields',
	);
	foreach ( $files as $file ) {
		require plugin_dir_path( __FILE__ ) . 'includes/' . $file . '.php';
	}
}

sixtenpressblockeditor_require();

$sixtenpressblockeditor_output        = new SixTenPressBlockEditorOutputEnqueue();
$sixtenpressblockeditor_register      = new SixTenPressBlockEditorPostType();
$sixtenpressblockeditor_fields        = new SixTenPressBlockEditorCustomFields();
$sixtenpressblockeditor_theme_support = new SixTenPressBlockEditorThemeSupport();
$sixtenpressblockeditor_filters       = new SixTenPressBlockEditorOutputFilters();
$sixtenpressblockeditor               = new SixTenPressBlockEditor(
	$sixtenpressblockeditor_output,
	$sixtenpressblockeditor_register,
	$sixtenpressblockeditor_fields,
	$sixtenpressblockeditor_theme_support,
	$sixtenpressblockeditor_filters
);

$sixtenpressblockeditor->init();

/**
 * Helper function to get the plugin setting, optionally just part of it.
 *
 * @param string $key
 * @return mixed|void|null
 */
function sixtenpressblockeditor_get_setting( $key = '' ) {
	$setting = apply_filters( 'sixtenpressblockeditor_get_setting', array() );

	return $key ? $setting[ $key ] : $setting;
}

/**
 * Helper function to get arrayed theme support values
 *
 * @param string $feature
 * @return array|boolean
 * @since 1.0.0
 */
function sixtenpressblockeditor_get_theme_support_array( $feature = 'editor-color-palette' ) {
	list( $theme, ) = (array) get_theme_support( $feature );

	return $theme;
}
