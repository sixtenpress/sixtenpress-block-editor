'use strict';

var gulp = require( 'gulp' );

gulp.task( 'watch', () => {
	var config = require( '../config' ),
		files = [
			config.output.styleDestination + '**/*.css',
			config.output.scriptDestination + '**/*.js'
		],
		browserSync = require( 'browser-sync' ).create();

	browserSync.init( files, {
		open: false,             // Open project in a new tab?
		injectChanges: true,     // Auto inject changes instead of full reload
		watchOptions: {
			debounceDelay: 1000  // Wait 1 second before injecting
		},
		proxy: config.url,
	} );

	gulp.watch( config.paths.jsPath, gulp.series( 'js' ) );
	gulp.watch( config.paths.sassPath, gulp.parallel( 'sass' ) );
} );
