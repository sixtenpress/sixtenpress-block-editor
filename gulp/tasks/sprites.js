'use strict';

var gulp = require( 'gulp' );

gulp.task(
	'sprites',
	() => {
		var svgSprite = require( 'gulp-svg-sprites' );

		return gulp.src( 'images/svg/*.svg' )
			.pipe( svgSprite( {
				svg: {
					symbols: "icons.svg"
				},
				mode: 'symbols',
				preview: false
			} ) )
			.pipe( gulp.dest( "images/sprites/" ) );
	}
);
